package Authorization;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 26.03.13
 * Time: 17:03
 * To change this template use File | Settings | File Templates.
 */
public class User {
    private String email;
    private String password;
    private Token token;

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }
}
