package Authorization;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 */
//https://oauth.vk.com/authorize?client_id=3483873&redirect_uri=http://oauth.vk.com/blank.html&scope=12&display=mobile&response_type=token
public class App {
    public static final String AUTHORIZATION_URL = "https://login.vk.com/?act=login";
    public static final String RESPONSE_HEADER = "Location";

    public static void main(String[] args) throws IOException {
        final HttpClient httpClient = new DefaultHttpClient();
        HttpGet get = new HttpGet(getURLAuthorization(httpClient));
        HttpResponse httpRequest = httpClient.execute(get);
        clearBufferRequest(httpRequest);
        System.out.println(getTokenURL(httpClient));
        System.out.println(getToken(httpClient));
    }
    private static String getToken(final HttpClient httpClient) throws IOException {
        final HttpPost getTokenPostMethod = new HttpPost(getTokenURL(httpClient));
        final HttpResponse tokenResponse = httpClient.execute(getTokenPostMethod);

        clearBufferRequest(tokenResponse);
        String locationHeader = tokenResponse.getFirstHeader(RESPONSE_HEADER).toString().split("\\#")[1];

        final String lifetimeOfToken = locationHeader.split("\\=")[2].split("\\&")[0]; //time
        final String userId = locationHeader.split("\\=")[3]; //id
        final String token = locationHeader.split("\\=")[1].split("\\&")[0];

        return locationHeader;
    }

    private static String getTokenURL(final HttpClient httpClient) throws IOException {
        final String urlForToken="https://oauth.vk.com/authorize?client_id=3483873&redirect_uri=http://oauth.vk.com/blank.html&scope=123&display=mobile&response_type=token";
        final HttpGet getTokenMethod = new HttpGet(urlForToken);
        final HttpResponse response = httpClient.execute(getTokenMethod);
        final BufferedReader tokenUrlReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String tokenUrl = new String();
        String tempLine = "";
        while ((tempLine = tokenUrlReader.readLine()) != null) {
            if (tempLine.indexOf("<form method=\"post\" action=") >= 0) {
                tokenUrl = tempLine.substring(tempLine.indexOf("https"), tempLine.length() - 2);
            }
        }
        return tokenUrl;
    }

    private static void clearBufferRequest(final HttpResponse response) throws IOException {
        final BufferedReader responseContent = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String line;
        while ((line = responseContent.readLine()) != null) {

        }
    }

    private static String getURLAuthorization(final HttpClient httpClient) throws IOException {
        final HttpPost authorizationMethod = new HttpPost(AUTHORIZATION_URL);
        final List<NameValuePair> authorizationAttributes = new ArrayList<NameValuePair>(1);
        authorizationAttributes.add(new BasicNameValuePair("email", "+375293205652"));
        authorizationAttributes.add(new BasicNameValuePair("pass", "86095785v"));
        authorizationMethod.setEntity(new UrlEncodedFormEntity(authorizationAttributes));

        final HttpResponse authorizationResponse = httpClient.execute(authorizationMethod);
        final String location = authorizationResponse.getFirstHeader("Location").toString();

        clearBufferRequest(authorizationResponse);
        return (location.split("\\:")[1].trim() + ":" + location.split("\\:")[2]).trim();
    }
}
