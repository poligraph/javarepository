package Authorization;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class VkOAuthAuthorization {
    private static final String AUTHORIZATION_URL = "https://login.vk.com/?act=login";
    private static final String RESPONSE_HEADER = "Location";
    private static final String USERNAME_FIELD = "email";
    private static final String PASSWORD_FIELD = "pass";
    private static final String REDIRECT_URL = "http://oauth.vk.com/blank.html";
    private static final String AUTHORIZATION_LOCATION_ERROR = "Location: https://vk.com";

    public static void getToken(final User user, final Application application) throws IOException {
        final HttpClient httpClient = new DefaultHttpClient();

        final String urlAuthorization = getURLAuthorization(httpClient, user);
        initHttpClient(httpClient, user, urlAuthorization);
        Token token = requestForToken(httpClient, user, application);
        user.setToken(token);
    }

    private static void initHttpClient(final HttpClient httpClient, final User user, final String urlAuthorization) throws IOException {
        final HttpGet get = new HttpGet(getURLAuthorization(httpClient, user));
        final HttpResponse httpRequest = httpClient.execute(get);
        clearBufferRequest(httpRequest);
    }

    private static Token requestForToken(final HttpClient httpClient, final User user, final Application application) throws IOException {
        final HttpPost tokenPostMethod = new HttpPost(getTokenURL(httpClient, user, application));
        final HttpResponse tokenResponse = httpClient.execute(tokenPostMethod);
        clearBufferRequest(tokenResponse);
        String locationHeader = tokenResponse.getFirstHeader(RESPONSE_HEADER).toString();
        if (locationHeader.equalsIgnoreCase(AUTHORIZATION_LOCATION_ERROR)) {
            throw new IllegalStateException("Invalid username or password");
        }
        locationHeader = locationHeader.split("\\#")[1];

        final Integer lifetimeOfToken = Integer.parseInt(locationHeader.split("\\=")[2].split("\\&")[0]); //time
        final String userId = locationHeader.split("\\=")[3]; //id
        final String token = locationHeader.split("\\=")[1].split("\\&")[0];
        return new Token(token, lifetimeOfToken);
    }

    private static String getURLAuthorization(final HttpClient httpClient, final User user) throws IOException {
        final HttpPost authorizationMethod = new HttpPost(AUTHORIZATION_URL);
        final List<NameValuePair> authorizationAttributes = new ArrayList<NameValuePair>(1);

        authorizationAttributes.add(new BasicNameValuePair(USERNAME_FIELD, user.getEmail()));
        authorizationAttributes.add(new BasicNameValuePair(PASSWORD_FIELD, user.getPassword()));
        authorizationMethod.setEntity(new UrlEncodedFormEntity(authorizationAttributes));

        final HttpResponse authorizationResponse = httpClient.execute(authorizationMethod);
        final String location = authorizationResponse.getFirstHeader(RESPONSE_HEADER).toString();

        clearBufferRequest(authorizationResponse);
        return (location.split("\\:")[1].trim() + ":" + location.split("\\:")[2]).trim();
    }

    private static void clearBufferRequest(final HttpResponse response) throws IOException {
        final BufferedReader responseContent = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        while (responseContent.readLine() != null) {

        }
    }

    private static String getTokenURL(final HttpClient httpClient, final User user, final Application application) throws IOException {
        String permissions = permissionsToString(application.getPermissions());
        final String urlForToken = "https://oauth.vk.com/authorize?client_id=" + application.getId() + "&redirect_uri=" + REDIRECT_URL + "&scope=" +"1"+ "&display=mobile&response_type=token";
        final HttpGet getTokenMethod = new HttpGet(urlForToken);
        final HttpResponse response = httpClient.execute(getTokenMethod);
        final BufferedReader tokenUrlReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String tokenUrl = new String();
        String tempLine = "";
        while ((tempLine = tokenUrlReader.readLine()) != null) {
            if (tempLine.indexOf("<form method=\"post\" action=") >= 0) {
                tokenUrl = tempLine.substring(tempLine.indexOf("https"), tempLine.length() - 2);
            }
        }
        return tokenUrl;
    }

    private static String permissionsToString(List<Permissions> permissions) {
        String resultString = new String();
        for (Permissions permission : permissions) {
            resultString += permission.toString().toLowerCase() + ",";
        }
        resultString =resultString.substring(0,resultString.length()-1);
        return resultString.replace(" ", ",");
    }

}
