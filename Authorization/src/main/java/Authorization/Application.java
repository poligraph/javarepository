package Authorization;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 26.03.13
 * Time: 17:17
 * To change this template use File | Settings | File Templates.
 */
public class Application {
    private int applicationId;
    private List<Permissions> permissions;
    public Application(int applicationId, Permissions... permissions) {
        this.applicationId = applicationId;
        this.permissions = new ArrayList<Permissions>();
        for (Permissions permission : permissions) {
            this.permissions.add(permission);
        }
    }

    public List<Permissions> getPermissions() {
        return permissions;
    }

    public int getId() {
        return applicationId;
    }
}
