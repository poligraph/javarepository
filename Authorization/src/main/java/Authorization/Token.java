package Authorization;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 26.03.13
 * Time: 19:31
 * To change this template use File | Settings | File Templates.
 */
public class Token {
    private int lifetimeOfToken;
    private String token;

    public Token(String token, int lifetimeOfToken) {
        this.token = token;
        this.lifetimeOfToken = lifetimeOfToken;
    }

    public String getToken() {
        return token;
    }

    public int getLifetimeOfToken() {
        return lifetimeOfToken;
    }
}
