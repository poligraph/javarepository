unit ExcludingOR;

interface

function encryptToXOR(text, key: string): string;
implementation

function encryptToXOR: string;
var
  i, j: integer;
begin
  for i := 1 to Length(text) do 
  begin
    for j := 1 to Length(key) do 
    begin
      text[i] := chr(ord(text[i]) xor ord(key[j]));
    end;
  end;
  encryptToXOR := text;
end;

begin
end. 