unit FeistelNetwork;

interface

procedure encrypt(l, r, n: byte);
procedure f(n: byte; flag: boolean);
procedure decrypt(l, r, n: byte);
function getL():byte;
function getR():byte;
implementation

var
  R, L: byte;

procedure f(n: byte; flag: boolean);
var
  temp: byte;
begin
  temp := L;
  if flag = true then begin
    L := R xor ((L + n) mod 128);
    R := temp;
  end
  else
    R := R xor ((L + n) mod 128);
end;

procedure encrypt(l, r, n: byte);
var
  i: byte;
begin
  R := r;
  L := l;
  for i := 1 to n do 
  begin
    if i < n then
      f(i, true)
    else
      f(i, false);
  end;
end;

procedure decrypt(l, r, n: byte);
var
  i: byte;
begin
  R := r;
  L := l;
  for i := n to 1 do 
  begin
    if i > 1 then
      f(i, true)
    else
      f(i, false);
  end;
end;
function getL():byte;
begin
getL:=L;
end;
function getR():byte;
begin
getR:=R;
end;
end.