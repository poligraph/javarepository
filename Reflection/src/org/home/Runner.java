package org.home;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Runner {
    public static void main(String[] args) throws IllegalAccessException, InstantiationException, IOException {
        List<Class<?>> pluginList = new ArrayList();
        for (int i = 0; i < 10; i++) {
            if (i % 2 == 0) {
                pluginList.add(SamplePlugin.class);
            } else {
                pluginList.add(ErrorPlugin.class);
            }
        }
        pluginList.add(String.class);
        List<Class> result = ClassSearcher.findInheritFrom(pluginList, Plugin.class);
        System.out.println(result.size());
        for (Class aClass : result) {
            Plugin samplePlugin = (Plugin) aClass.newInstance();
            System.out.println(samplePlugin.getPluginName());
        }
        JarFilesScanner.findClasses("Reflection.jar");
    }
}
