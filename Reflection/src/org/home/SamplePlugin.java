package org.home;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 19.02.13
 * Time: 20:44
 * To change this template use File | Settings | File Templates.
 */
public class SamplePlugin extends Plugin implements PluginInfo {
    private String authorName="Developer";

    @Override
    public String getAuthor() {
        return authorName;
    }

    @Override
    public void setAuthor(String author) {
        this.authorName = author;
    }
}
