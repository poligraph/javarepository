package org.home;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarFile;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 08.03.13
 * Time: 19:51
 * To change this template use File | Settings | File Templates.
 */
public class JarFilesScanner {
    public static final String PACKAGE_SEPARATOR = "\\/";
    private static final String EXTENSION_CLASS_FILE = ".class";

    public static List<String> findClasses(String jarFileName) throws IOException {
        List<String> classFileNames = new ArrayList<String>();
        JarFile jarFile = new JarFile(jarFileName);
        Enumeration enumeration = jarFile.entries();
        while (enumeration.hasMoreElements()) {
            String element = enumeration.nextElement().toString();
            if (element.endsWith(EXTENSION_CLASS_FILE)) {
                element = element.substring(0, element.length() - EXTENSION_CLASS_FILE.length()).replaceAll(PACKAGE_SEPARATOR, ".");
                classFileNames.add(element);
            }
        }
        return classFileNames;
    }
}
