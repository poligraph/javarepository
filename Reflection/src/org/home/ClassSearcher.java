package org.home;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 08.03.13
 * Time: 17:42
 * To change this template use File | Settings | File Templates.
 */
public class ClassSearcher {
    public static List<Class> findImplementInterface(List<Class<?>> classes, Class<?> realize) {
        List resultList = new ArrayList();
        for (Class investigatedClass : classes) {
            Class[] interfaces = investigatedClass.getInterfaces();
            if (Arrays.asList(interfaces).contains(realize)) {
                resultList.add(investigatedClass);
            }
        }
        return resultList;
    }

    public static List<Class> findInheritFrom(List<Class<?>> classes, Class<?> superclass) {
        List<Class> resultList = new ArrayList();
        for (Class<?> investigatedClass : classes) {
            if (investigatedClass.getSuperclass().equals(superclass)) {
                resultList.add(investigatedClass);
            }
        }
        return resultList;
    }
}
