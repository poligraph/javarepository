import java.sql.*;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 03.12.12
 * Time: 20:45
 * To change this template use File | Settings | File Templates.
 */
public class SQLiteWork {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Class.forName("org.sqlite.JDBC");
        Connection connection= DriverManager.getConnection("jdbc:sqlite:Sample.sqlite");
        Statement statement=connection.createStatement();
        statement.execute("CREATE TABLE IF NOT EXISTS test(name,text)");
        PreparedStatement preparedStatement=connection.prepareStatement("INSERT INTO test VALUES (?,?)");
        preparedStatement.setString(1,"Key1");
        preparedStatement.setString(2,"Значение");
        preparedStatement.addBatch();
        connection.setAutoCommit(false);
        preparedStatement.executeBatch();
        connection.setAutoCommit(true);
        ResultSet resultSet=statement.executeQuery("SELECT * FROM test;");
        while (resultSet.next()){
            System.out.println("KEY: "+resultSet.getString("name")+", VALUE: "+resultSet.getString("text"));
        }

    }
}
