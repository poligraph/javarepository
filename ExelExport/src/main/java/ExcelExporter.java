import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.*;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 08.12.12
 * Time: 16:00
 * To change this template use File | Settings | File Templates.
 */
public class ExcelExporter {
    private int counter=0;
    private WritableWorkbook writableWorkbook;
    private WritableCellFormat writableCellFormat;

    public ExcelExporter(String fileName) throws IOException {
        WorkbookSettings workbookSettings = new WorkbookSettings();
        workbookSettings.setLocale(new Locale("ru", "RU"));
        writableWorkbook = Workbook.createWorkbook(new File(fileName),workbookSettings);
    }
    public void writeInExcel(int i,int j,String value,String sheetName) throws WriteException {
        WritableFont arial=new WritableFont(WritableFont.ARIAL,12,WritableFont.NO_BOLD);
        writableCellFormat=new WritableCellFormat(arial);
        writableCellFormat.setAlignment(Alignment.JUSTIFY);
        writableCellFormat.setWrap(true);
        Label label=new Label(j,i,value,writableCellFormat);
        writableWorkbook.getSheet(sheetName).addCell(label);
    }
    public void createSheet(String sheetName){
        writableWorkbook.createSheet(sheetName,counter++);
    }
    public void closeExcel() throws IOException, WriteException {
        writableWorkbook.write();
        writableWorkbook.close();
    }
}
