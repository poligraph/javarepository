import jxl.write.WriteException;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 08.12.12
 * Time: 16:28
 * To change this template use File | Settings | File Templates.
 */
public class ExcelExporterTest {
    private ExcelExporter excelExporter;
    @Before
    public void setUp() throws Exception {
        excelExporter=new ExcelExporter("export.xls");
    }
    @Test
    public void test() throws WriteException, IOException {
        excelExporter.createSheet("Солдаты");
        excelExporter.createSheet("TwoList");
        excelExporter.writeInExcel(0,1,"Привет","Солдаты");
        excelExporter.writeInExcel(5,5,"Abra-Cadabra","TwoList");
        excelExporter.closeExcel();
    }
}
