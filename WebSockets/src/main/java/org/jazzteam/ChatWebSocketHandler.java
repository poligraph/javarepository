package org.jazzteam;

import org.eclipse.jetty.websocket.WebSocket;
import org.eclipse.jetty.websocket.WebSocketHandler;

import java.io.IOException;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * Created by IntelliJ IDEA.
 * User: Evgeniy
 * Date: 16.09.12
 * Time: 12:59
 * To change this template use File | Settings | File Templates.
 */
public class ChatWebSocketHandler extends WebSocketHandler {
    private final Set<ChatWebSocket> webSockets = new CopyOnWriteArraySet<ChatWebSocket>();

    @Override
    public WebSocket doWebSocketConnect(javax.servlet.http.HttpServletRequest httpServletRequest, String s) {

        return new ChatWebSocket();
    }

    private class ChatWebSocket implements WebSocket.OnTextMessage {
        private Connection connection;

        @Override
        public void onOpen(Connection connection) {
            this.connection = connection;
            webSockets.add(this);
        }

        @Override
        public void onMessage(String data) {
            try {
                for (ChatWebSocket webSocket : webSockets) {
                    webSocket.connection.sendMessage(data);
                }
            } catch (IOException e) {
                connection.disconnect();
            }
        }

        @Override
        public void onClose(int closeCode, String message) {
            webSockets.remove(this);
        }
    }

}

