package org.jazzteam;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.DefaultHandler;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created by IntelliJ IDEA.
 * User: Evgeniy
 * Date: 16.09.12
 * Time: 13:12
 * To change this template use File | Settings | File Templates.
 */
public class ChatServerServletContextListener implements ServletContextListener {
    private Server server = null;

    @Override
    public void contextInitialized(ServletContextEvent event) {
        try {
            this.server = new Server(8081);
            ChatWebSocketHandler chatWebSocketHandler = new ChatWebSocketHandler();
            chatWebSocketHandler.setHandler(new DefaultHandler());
            server.setHandler(chatWebSocketHandler);
            server.start();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        if (server != null) {
            try {
                server.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
