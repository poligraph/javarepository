/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 15.02.13
 * Time: 11:33
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function () {
    socket = new WebSocket("ws://Evgenii:8081/");
    socket.onmessage = function (msg) {
        var jsonText = JSON.parse(JSON.stringify(msg));
        var data=jsonText.data.split("%");
        showMessage(data[0],data[1]);
    };
    socket.onopen = function () {
    };
    socket.onclose = function () {
    };
});

function showMessage(user,message) {
    var userInfoNode = document.createElement("p");
    var messageTextNode = document.createElement("span");
    var messageNode = document.createElement("div");
    userInfoNode.className = "userInfo";
    messageNode.className = "tooltip";
    userInfoNode.textContent = "[" + user + "]" + getDate();
    messageNode.appendChild(userInfoNode);
    messageTextNode.textContent = message;
    messageNode.appendChild(messageTextNode);
    var history = document.getElementById("history");
    history.appendChild(messageNode);
}
function getDate() {
    var date = new Date();
    var hour = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
    var minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
    var seconds = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
    return hour + ":" + minutes + ":" + seconds;
}
function sendMessage() {
    var messageNode = document.getElementById("messageText");
    if (messageNode.textContent.length > 0) {
        socket.send(userName+"%"+messageNode.textContent);
        messageNode.textContent = "";
    }
}
userName = "Guest";
function enterUserName() {
    var name = prompt("Enter User Name", "");
    if (name.length != 0) {
        userName = name;
    }
}
