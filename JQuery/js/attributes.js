$(function(){
	// Добавление класса
	$("ul").addClass("list");
	// Удаление класса
	$("table").removeClass("list");
	// Добавляет класс, или удаляет если он уже добавлен
	$("table").toggleClass("list");
	// Проверка, добавлен ли класс
	$("li").hasClass("list");
	$("li:even").css({color:"white"});

	// Проверка отмечен ли флажок
	$("input[type='checkbox']").prop("checked");
	$("input[type='checkbox']").is(":checked");
	// Отметить флажек
	$("input[type='checkbox']").prop({checked:true});
	$("input[type='checkbox']").attr({checked:true});
	// Установить аттрибут title
	$("input[type='checkbox']").prop({title:"Переключатель"});
	$("input[type='checkbox']").attr({title:"Переключатель"});
	// Удалить аттрибут title
	$("input[type='checkbox']").removeAttr("title");
});