$(function(){
	// ВСТАВКА ЭЛЕМЕНТОВ
	// вставка перед элементом
	var beforeItem=$("<li>Before Item 1</li>");
	$("ul li:contains('One')").before(beforeItem); 
	$("<li>Before Item 2</li>").insertBefore("ul li:contains('One')");
	// Вставка после элемента
	var afterItem=$("<li>After Item 1</li>");
	$("ul li:contains('One')").after(afterItem);
	$("<li>After Item 2</li>").insertAfter("ul li:contains('One')");
	// Добавить дочерний элемент в начало родительского
	$("<li>Start Children</li>").prependTo("ul");
	// Добавить дочерний элемент в конец родительского
	$("<li>End Children</li>").appendTo("ul");
	// Оборачивает элементы заданными html-элементами
	$(beforeItem).wrap("<b></b>");
	//Удаляет родительский элемент, оставляя текущий элемент на прежнем месте 
	$(beforeItem).unwrap();

	// УДАЛЕНИЕ ЭЛЕМЕНТОВ
	// Удаляет элементы со страницы
	$("tr:first").remove();
	$("tr:first").detach();
	// Удаляет все дочерние элементы
	$("table").empty();

// КЛОНИРОВАНИЕ ЭЛЕМЕНТОВ 
var cloneBeforeItem=$(beforeItem).clone();
$("ul").append(cloneBeforeItem);
});