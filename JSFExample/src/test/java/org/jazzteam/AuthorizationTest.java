package org.jazzteam;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 11.02.13
 * Time: 14:27
 * To change this template use File | Settings | File Templates.
 */
public class AuthorizationTest {
    private Authorization authorization;

    @Before
    public void setUp() throws Exception {
        authorization = new Authorization();
}

    @Test
    public void test1() {
        assertTrue(authorization.getUserName().equals("Guest"));
        assertFalse(authorization.isAuthorized());
        authorization.setUserName("Admin");
        authorization.setHashPassword("root");
        assertTrue(authorization.logIn().equals(Authorization.SUCCESS_AUTHORIZATION_PAGE));
        assertTrue(authorization.isAuthorized());
        assertTrue(authorization.getUserName().equals("Admin"));
        authorization.logOut();
        assertFalse(authorization.isAuthorized());
        assertTrue(authorization.getUserName().equals("Guest"));
    }
}
