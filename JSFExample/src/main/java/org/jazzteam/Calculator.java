package org.jazzteam;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 07.02.13
 * Time: 11:56
 * To change this template use File | Settings | File Templates.
 */
@ManagedBean(name="calculator")
@RequestScoped
public class Calculator implements Serializable  {
    private int firstNumber;
    private int result;
    private int secondNumber;

    public Calculator() {
        firstNumber=0;
        secondNumber=0;
        result=0;
    }

    public int getFirstNumber() {
        return firstNumber;
    }

    public void setFirstNumber(int firstNumber) {
        this.firstNumber = firstNumber;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public int getSecondNumber() {
        return secondNumber;
    }

    public void setSecondNumber(int secondNumber) {
        this.secondNumber = secondNumber;
    }

    public void add() {
        result = firstNumber + secondNumber;
    }

    public void multiply() {
        result = firstNumber * secondNumber;
    }

    public void clear() {
        result = 0;
    }

}
