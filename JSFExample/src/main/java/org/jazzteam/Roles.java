package org.jazzteam;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 11.02.13
 * Time: 10:39
 * To change this template use File | Settings | File Templates.
 */
@ManagedBean(name = "roles")
@ApplicationScoped
public class Roles {
    private String userRole;

    public Roles() {
        userRole = "User";
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }
}
