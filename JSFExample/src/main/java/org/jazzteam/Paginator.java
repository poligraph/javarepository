package org.jazzteam;

import javax.faces.bean.ManagedBean;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 14.02.13
 * Time: 13:57
 * To change this template use File | Settings | File Templates.
 */
@ManagedBean
public class Paginator implements Serializable {
    private String requestParameter;

    public Paginator() {
    }

    public String getRequestParameter() {
        return requestParameter;
    }

    public void setRequestParameter(String requestParameter) {
        this.requestParameter = requestParameter;
    }
}
