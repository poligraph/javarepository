package org.jazzteam;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 11.02.13
 * Time: 14:03
 * To change this template use File | Settings | File Templates.
 */
@ManagedBean
@SessionScoped
public class Authorization implements Serializable {
    public static final String SUCCESS_AUTHORIZATION_PAGE = "success";
    public static final String ERROR_AUTHORIZATION_PAGE = "error";
    public static final String GUEST_NAME = "Guest";
    private static final Map<String, Integer> userData = new HashMap<String, Integer>();
    public static final String ENTER_S4CEESS = "templates/userInfo.xhtml";
    private String userName;
    private String userPanel = "templates/login.xhtml";
    private boolean authorized;
    private String hashPassword;

    public Authorization() {
        authorized = false;
        userName = GUEST_NAME;
        userData.put("Admin", "root".hashCode());
        userData.put("Vasya", "qwerty".hashCode());
    }

    public String getUserPanel() {
        if(!isAuthorized()){
        return userPanel;
        }
        return  "templates/login.xhtml";
    }

    public void setUserPanel(String userPanel) {
        this.userPanel = userPanel;
    }

    public String getHashPassword() {
        return hashPassword;
    }

    public void setHashPassword(String hashPassword) {
        this.hashPassword = hashPassword;
    }

    public boolean isAuthorized() {
        return authorized;
    }

    public String getUserName() {
        if (isAuthorized())
            return userName;
        else
            return GUEST_NAME;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String logIn() {
        if (userData.containsKey(userName.trim())) {
            if (userData.get(userName) == hashPassword.hashCode()) {
                this.authorized = true;
                return SUCCESS_AUTHORIZATION_PAGE;
            }
        }
        this.authorized = false;
        this.userName = GUEST_NAME;
        this.hashPassword = "";

        return ERROR_AUTHORIZATION_PAGE;
    }

    public void logOut() {
        this.authorized = false;
        this.userName = GUEST_NAME;
        this.hashPassword = "";
    }
}
