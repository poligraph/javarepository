package org.jazzteam;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 07.02.13
 * Time: 14:35
 * To change this template use File | Settings | File Templates.
 */
@SessionScoped
@ManagedBean(name = "user")
public class User {
private String name;
@ManagedProperty(value = "#{roles}")
private Roles roles;
private String role;

    public User() {
        name = "Admin";
    }

    public Roles getRoles() {
        return roles;
    }

    public void setRoles(Roles roles) {
        this.roles = roles;
    }

    public String getRole() {
        if (roles != null)
            role = roles.getUserRole();
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
