package org.jazzteam;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 11.02.13
 * Time: 16:52
 * To change this template use File | Settings | File Templates.
 */
@ManagedBean
@RequestScoped
public class Navigator implements Serializable {
    List<String> strings=new ArrayList<String>();
    public String moveToHome(){
        return "users";
    }
}
