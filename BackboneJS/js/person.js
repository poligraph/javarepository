(function(){
window.App={};
window.App.Model={};
window.App.View={};
window.App.Collection={};

App.Model.Person =Backbone.Model.extend({
	initialize:function(){

	},
	defaults:{
		name:"User",
		age:"unknown"
	}
});

App.View.Person=Backbone.View.extend({
	tagName:"li",
	id:"person-item",
	className:"list",
	template:"#template",
	render:function(){
		var template=_.template($(this.template).html());
		this.$el.html(template(this.model.toJSON()));
		return this;
	}
});

App.View.People=Backbone.View.extend({
	tagName:"ul",
	initialize:function(){
	},
	render:function(){
		this.collection.each(function(person){
			var personView=new App.View.Person({model:person});
			this.$el.append(personView.render().el);
		},this);
		return this;
	}
});

App.Collection.Person=Backbone.Collection.extend({
	model:App.Model.Person
});

})();
var people=[{
	name:"Евгений"
},{
	name:"Алексей",
	age:23
},{
	name:"Сергей",
	age:27
}];

var personCollection=new App.Collection.Person(people);
var peopleView=new App.View.People({collection:personCollection});

