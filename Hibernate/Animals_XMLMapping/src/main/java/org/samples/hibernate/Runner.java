package org.samples.hibernate;

import org.hibernate.Session;

public class Runner {
    public static void main(String[] args) {
        Session session = HibernateUtill.getSessionFactory().openSession();

        session.beginTransaction();
        Animal animal = new Animal();

        animal.setName("Matroskin");
        animal.setAge(5);

        session.save(animal);
        session.getTransaction().commit();
    }
}
