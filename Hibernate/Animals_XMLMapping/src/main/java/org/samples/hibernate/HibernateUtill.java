package org.samples.hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtill {
    private static final SessionFactory factory;

    static {
        factory = buildSessionFactory();
    }

    public static SessionFactory getSessionFactory() {
        return factory;
    }

    private static SessionFactory buildSessionFactory() {
        return new Configuration().configure().buildSessionFactory();
    }

    public static void shutDown() {
        getSessionFactory().close();
    }
}
