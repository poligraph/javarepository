package org.samples.hibernate;

import java.util.List;

public interface AnimalDAO {
    void addAnimal(Animal animal);

    void updateAnimal(Animal animal);

    Animal getAnimalById(int id);

    List<Animal> getAllAnimals();

    void deleteAnimal(Animal animal);

    List<Animal> getAnimalByName(String likeExpression);
    Animal getAnimalByExampleObject(Animal exampleObject);
}
