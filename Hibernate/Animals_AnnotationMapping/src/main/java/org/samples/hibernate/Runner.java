package org.samples.hibernate;

import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;

public class Runner {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure();
        SchemaExport export=new SchemaExport(configuration);
        export.setOutputFile("Zoo.sql");
        export.create(false,true);
    }
}
