package org.samples.hibernate;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import java.util.ArrayList;
import java.util.List;

public class AnimalDAOImpl implements AnimalDAO {
    @Override
    public void addAnimal(Animal animal) {
        Session session = null;
        try {
            session = HibernateUtill.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(animal);
            session.getTransaction().commit();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public void updateAnimal(Animal animal) {
        Session session = null;
        try {
            session = HibernateUtill.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(animal);
            session.getTransaction().commit();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public Animal getAnimalById(int id) {
        Session session = null;
        Animal animal = null;
        try {
            session = HibernateUtill.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Animal.class);
            criteria.add(Expression.eq("id", id));
            animal = (Animal) criteria.uniqueResult();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return animal;
    }

    @Override
    public List<Animal> getAllAnimals() {
        Session session = null;
        List<Animal> animals = new ArrayList<Animal>();
        try {
            session = HibernateUtill.getSessionFactory().openSession();
            animals = session.createCriteria(Animal.class).addOrder(Order.asc("id")).list();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return animals;
    }

    @Override
    public void deleteAnimal(Animal animal) {
        Session session = null;
        try {
            session = HibernateUtill.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(animal);
            session.getTransaction().commit();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public List<Animal> getAnimalByName(String likeExpression) {
        Session session = null;
        List<Animal> animals = new ArrayList<Animal>();
        try {
            session = HibernateUtill.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Animal.class);
            criteria.add(Expression.like("name", likeExpression));
            animals = criteria.list();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return animals;
    }

    @Override
    public Animal getAnimalByExampleObject(Animal exampleObject) {
        Session session = null;
        Animal animal = null;
        try {
            session = HibernateUtill.getSessionFactory().openSession();
            Example example = Example.create(exampleObject).enableLike().excludeNone().ignoreCase();
            animal = (Animal) session.createCriteria(Animal.class).add(example).uniqueResult();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return animal;
    }
}
