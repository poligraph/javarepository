
    drop table if exists animals

    create table animals (
        id integer not null,
        age integer,
        name varchar(64),
        primary key (id)
    ) ENGINE=InnoDB
