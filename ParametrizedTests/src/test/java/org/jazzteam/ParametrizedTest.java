package org.jazzteam;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static junit.framework.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 08.02.13
 * Time: 13:37
 * To change this template use File | Settings | File Templates.
 */
@RunWith(Parameterized.class)
public class ParametrizedTest {
    private int a;
    private int b;
    private boolean result;
    public ParametrizedTest(int a,int b,boolean result){
        this.a=a;
        this.b=b;
        this.result=result;
    }
    @Parameterized.Parameters
    public static Collection data(){
        return Arrays.asList(new Object[][]{{1,1,true},{2,2,false},{3,3,false}});
    }
    @Test
    public void test(){
        assertTrue((a==Math.pow(a,2))==result);
    }
}
