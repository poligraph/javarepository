package Mockito;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 22.02.13
 * Time: 20:30
 * To change this template use File | Settings | File Templates.
 */
public class MockitoTestRunner {
    private List mockOne = mock(ArrayList.class);
    private List mockSecond=mock(ArrayList.class);
    private InOrder order;

    @Before
    public void init() {
        order=inOrder(mockOne,mockSecond);
    }

    @Test
    public void test() {
        mockOne.get(anyInt());
        mockSecond.add(anyInt());
        order.verify(mockOne).get(anyInt());
        order.verify(mockSecond).add(anyInt());
        verifyZeroInteractions(mockOne,mockSecond);
    }
}
