package Mockito;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 22.02.13
 * Time: 20:18
 * To change this template use File | Settings | File Templates.
 */
public class BarVoid {
    private FooVoid foo;
    public BarVoid(FooVoid foo){
        this.foo=foo;
    }
    public void bar(String parameter){
        foo.foo(parameter);
    }
}
