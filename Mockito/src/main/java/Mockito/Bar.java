package Mockito;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 22.02.13
 * Time: 20:16
 * To change this template use File | Settings | File Templates.
 */
public class Bar {
    private Foo foo;
    public Bar(Foo foo){
        this.foo=foo;
    }
    public String bar(String parameter){
        return foo.foo(parameter);
    }

}
