package org.samples.spring;

public interface MessageProvider {
    String getMessage();

    void setMessage(String message);
}
