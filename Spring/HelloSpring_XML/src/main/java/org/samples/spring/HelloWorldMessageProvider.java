package org.samples.spring;

public class HelloWorldMessageProvider implements MessageProvider {
    private String message;

    @Override
    public String getMessage() {
        return "Hello " + message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }
}
