function ajax(type, url, callBack) {
    var xmlHttpRequest = new XMLHttpRequest();
    xmlHttpRequest.onreadystatechange = function callBackFunction() {
        if (xmlHttpRequest.readyState == 4) {
            if (xmlHttpRequest.status == 200) {
                var status = xmlHttpRequest.responseText;
                callBack(status);
            }
        }
    };
    xmlHttpRequest.open(type, url, true);
    xmlHttpRequest.send(null);
}
(function init() {
    ajax("GET", "http://test1.ru/IceCreamSundae.xml", function (data) {
        alert(data);
    });
})();