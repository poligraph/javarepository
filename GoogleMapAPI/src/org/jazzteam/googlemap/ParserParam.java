package org.jazzteam.googlemap;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.Map.Entry;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import java.net.URLEncoder;
import com.google.common.base.Functions;
import com.google.common.collect.Iterables;
/**
 * Created by IntelliJ IDEA.
 * User: Evgeniy
 * Date: 13.09.12
 * Time: 15:15
 * To change this template use File | Settings | File Templates.
 */
public class ParserParam {
    public static  String encodeParams(final Map<String,String> params){
        final String paramsUrl=Joiner.on("&").join(Iterables.transform(
                params.entrySet(),new Function<Map.Entry<String, String>, String>() {
            @Override
            public String apply(final Entry<String,String> input) {
                try{
                    final StringBuffer buffer=new StringBuffer();
                    buffer.append(input.getKey());
                    buffer.append('=');
                    buffer.append(URLEncoder.encode(input.getValue(),"utf-8"));
                    return buffer.toString();
                }
                catch (final UnsupportedEncodingException e){
                    throw new RuntimeException(e);
                }
            }
        }));
        return paramsUrl;
    }
}
