package org.jazzteam.googlemap;

import org.json.JSONObject;
import org.json.*;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;

/**
 * Created by IntelliJ IDEA.
 * User: Evgeniy
 * Date: 13.09.12
 * Time: 15:00
 * To change this template use File | Settings | File Templates.
 */
public class JsonReader {
    private static String readAll(final Reader rd) throws IOException{
        final StringBuilder sb=new StringBuilder();
        int cp;
        while ((cp=rd.read())!=-1){
            sb.append((char)cp);
        }
        return sb.toString();
    }
    public static JSONObject read(final String url) throws IOException,JSONException{
        final InputStream is=new URL(url).openStream();
        try{
            final BufferedReader rd=new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            final String jsonText=readAll(rd);
            final JSONObject json=new JSONObject(jsonText);
            return json;
        }
        finally {
            is.close();
        }
    }
}
