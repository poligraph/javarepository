package org.jazzteam.googlemap;

import com.google.common.collect.Maps;
import org.json.JSONException;

import java.io.IOException;
import java.util.Map;
import org.jazzteam.googlemap.ParserParam;
import org.json.JSONObject;

/**
 * Created by IntelliJ IDEA.
 * User: Evgeniy
 * Date: 13.09.12
 * Time: 15:51
 * To change this template use File | Settings | File Templates.
 */
public class RunClass {
    public static void main(final String[] args) throws IOException,JSONException{
        final String baseUrl="http://maps.googleapis.com/maps/api/geocode/json";
        final Map<String,String> params= Maps.newHashMap();
        params.put("sensor","false");
        params.put("address","ул В.Козлова, Солигорск, Минская область, Беларусь");
        final String url=baseUrl+"?"+ParserParam.encodeParams(params);
        System.out.println(url);
        final JSONObject response=JsonReader.read(url);
        JSONObject location=response.getJSONArray("results").getJSONObject(0);
        location =location.getJSONObject("geometry");
        location=location.getJSONObject("location");
        final double lng=location.getDouble("lng");
        final double lat=location.getDouble("lat");
        System.out.println("Широта:"+lat+", Долгота:"+lng);
    }
}
