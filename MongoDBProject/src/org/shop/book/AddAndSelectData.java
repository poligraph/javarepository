package org.shop.book;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by IntelliJ IDEA.
 * User: Evgeniy
 * Date: 29.09.12
 * Time: 12:20
 * To change this template use File | Settings | File Templates.
 */
public class AddAndSelectData {
    public static void main(String[] args) throws UnknownHostException {
        BookSession bSession=new BookSession();
        Books book=new Books();
        book.setName("PHP");
        book.setAuthor("Babura");
        book.setLanguage("Russian");
        book.setYear(2009);
        bSession.initDB();
        //bSession.createBook(book);
        List<Books> newBook=(ArrayList<Books>)bSession.getBooks();
        Books temp;
        for(int i=1;i<newBook.size();i++){
            temp=newBook.get(i);
            System.out.println("Name: "+temp.getName());
            System.out.println("Author: "+temp.getAuthor());
            System.out.println("Language: "+temp.getLanguage());
            System.out.println("Year: "+temp.getYear());
        }
    }
}
