package org.shop.book;

import com.mongodb.*;

import java.util.ArrayList;
import java.util.List;
import java.net.UnknownHostException;

/**
 * Created by IntelliJ IDEA.
 * User: Evgeniy
 * Date: 29.09.12
 * Time: 12:07
 * To change this template use File | Settings | File Templates.
 */
public class BookSession {
    private static final String dbName="BooksDatabases";
    private static final String docName="book";
    private DBCollection bookCollection;
    public void initDB() throws UnknownHostException {
        Mongo mongo=new Mongo();
        DB db=mongo.getDB(this.dbName);
        this.bookCollection=db.getCollection(this.docName);
        if(bookCollection==null){
            bookCollection=db.createCollection(this.docName,null);

        }
    }
    public void createBook(Books book){
        BasicDBObject doc=book.toDBObject();
        bookCollection.insert(doc);
    }
    public List<Books> getBooks(){
        List<Books> books=new ArrayList<Books>();
        DBCursor cursor=bookCollection.find();
        while(cursor.hasNext()){
            DBObject dbo=cursor.next();
            books.add(Books.fromDBObject(dbo));
        }
        return books;
    }

}
