package org.shop.book;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
/**
 * Created by IntelliJ IDEA.
 * User: Evgeniy
 * Date: 29.09.12
 * Time: 11:46
 * To change this template use File | Settings | File Templates.
 */

public class Books {
    private String name;
    private String autor;
    private String language;
    private int year;
    public void setName(String name){
        this.name=name;
    }
    public String getName(){
        return this.name;
    }
    public void setAuthor(String author){
        this.autor=author;
    }
    public String getAuthor(){
        return this.autor;
    }
    public void setLanguage(String language){
        this.language=language;
    }
    public String getLanguage(){
        return this.language;
    }
    public void setYear(int year){
        this.year=year;
    }
    public int getYear(){
        return this.year;
    }
    public BasicDBObject toDBObject(){
        BasicDBObject doc=new BasicDBObject();
        doc.put("name",name);
        doc.put("author",autor);
        doc.put("language",language);
        doc.put("year",year);
        return doc;
    }
   public static Books fromDBObject(DBObject document){
       Books book=new Books();
       book.name=(String)document.get("name");
       book.autor=(String)document.get("author");
       book.language=(String)document.get("language");
       book.year=Integer.parseInt(document.get("year").toString());
       return book;
   }
}
