import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: evgenii
 * Date: 11.12.12
 * Time: 0:25
 * To change this template use File | Settings | File Templates.
 */
public class RedisUseTest {
    private RedisUse redisUse;
    @Before
    public void setUp() throws Exception {
        redisUse=new RedisUse();
    }
    @Test
    public void test(){
        redisUse.set("name","Poligraph");
        redisUse.set("firstName","Sharicov");
        assertTrue(redisUse.get("name").equalsIgnoreCase("Poligraph"));
        assertTrue(redisUse.get("firstName").equalsIgnoreCase("Sharicov"));
    }
}
