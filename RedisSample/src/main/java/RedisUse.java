import redis.clients.jedis.Jedis;

/**
 * Created with IntelliJ IDEA.
 * User: evgenii
 * Date: 11.12.12
 * Time: 0:09
 * To change this template use File | Settings | File Templates.
 */
public class RedisUse {
    private Jedis jedis;
    public RedisUse(){
        jedis=new Jedis("localhost",6379);
        jedis.connect();
    }
    public void set(String key,String value){
        jedis.set(key,value);
    }
    public void set(String key,int value){
        jedis.set(key, String.valueOf(value));
    }
    public void inc(String key){
        jedis.incr(key);
    }
    public String get(String key){
        return jedis.get(key);
    }
}
