package org.home.swing.events;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 10.02.13
 * Time: 12:48
 * To change this template use File | Settings | File Templates.
 */
public class MouseEvents extends JFrame {
    public MouseEvents(){
        super("Mouse Events");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(300, 300));
        setSize(new Dimension(300,300));
        addMouseMotionListener(new MMoveListener());
        addMouseWheelListener(new MWheelListener());
        addWindowListener(new WindowListener());
        setVisible(true);
    }
    private class MMoveListener extends MouseMotionAdapter {
        @Override
        public void mouseMoved(MouseEvent events){
            System.out.println("Указатель переместился!");
        }
    }
    private class MWheelListener implements MouseWheelListener{

        @Override
        public void mouseWheelMoved(MouseWheelEvent e) {
            System.out.println("Прокрутка колесика");
        }
    }
    private class WindowListener extends WindowAdapter {
        @Override
        public void windowActivated(WindowEvent event){
            System.out.println("Окно активировано!");
        }
    }
}
