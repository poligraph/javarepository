package org.home.swing.events;

import org.home.swing.myevent.HButtonPressEvent;
import org.home.swing.myevent.HButtonPressListener;
import org.home.swing.myevent.HSimpleButton;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 10.02.13
 * Time: 12:16
 * To change this template use File | Settings | File Templates.
 */
public class KeyEvents extends JFrame {
    public KeyEvents() {
        super("Key Events");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addKeyListener(new KListener());
        setPreferredSize(new Dimension(300, 300));
        setSize(new Dimension(300, 300));
        setResizable(false);
        HSimpleButton button=new HSimpleButton();
        button.addButtonPressListener(new HButtonPressListener() {
            @Override
            public void hButtonPressed(HButtonPressEvent event) {
                System.out.println("Нажата кнопка");
            }
        });
        add(button);

                setVisible(true);
    }

    private class KListener implements KeyListener {
        @Override
        public void keyTyped(KeyEvent e) {
            System.out.println("Печать символа!");
        }

        @Override
        public void keyPressed(KeyEvent e) {
            System.out.println("Нажатие клавиши!");
        }

        @Override
        public void keyReleased(KeyEvent e) {
            System.out.println("Отжатие клавиши!");
        }
    }
}
