package org.home.swing.layouts;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 22.02.13
 * Time: 12:22
 * To change this template use File | Settings | File Templates.
 */
public class GridLayout extends JFrame {
    public GridLayout(){
        super("Frame");
        setSize(new Dimension(300,400));
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        JPanel content=new JPanel(new BorderLayout());
        JPanel jPanel=new JPanel(new java.awt.GridLayout(1,2,5,0));
        jPanel.add(new JButton("One"));
        jPanel.add(new JButton("Two"));
        JPanel jPanel1=new JPanel(new FlowLayout());
        jPanel.add(jPanel1);
        content.add(jPanel,BorderLayout.NORTH);
        setContentPane(content);
    }
}
