package org.home.swing.layouts;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 22.02.13
 * Time: 12:49
 * To change this template use File | Settings | File Templates.
 */
public class BoxLayoutSample extends JFrame {
    public BoxLayoutSample(){
        super("Frame");
        setSize(new Dimension(300,400));
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
       Box content=Box.createVerticalBox();
        getContentPane().add(content);
        Box hBox=Box.createHorizontalBox();
        hBox.add(Box.createHorizontalGlue());
        hBox.add(new JButton("Three"));
        hBox.add(Box.createHorizontalGlue());
        hBox.setVisible(true);
        content.add(hBox);
        content.add(Box.createVerticalGlue());
        content.add(new JButton("One"));
        content.add(Box.createVerticalStrut(10));
        content.add(new JButton("Two"));
        content.add(Box.createVerticalGlue());
        content.setVisible(true);

    }
}
