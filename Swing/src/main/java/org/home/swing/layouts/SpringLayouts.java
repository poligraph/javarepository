package org.home.swing.layouts;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 22.02.13
 * Time: 12:13
 * To change this template use File | Settings | File Templates.
 */
public class SpringLayouts extends JFrame {
    public SpringLayouts(){
        super("Frame");
        setSize(new Dimension(300,400));
        setVisible(true);
        SpringLayout springLayout=new SpringLayout();
        JPanel panel=new JPanel(springLayout);
        JButton button1=new JButton("One");
        JButton button2=new JButton("Two");
        panel.add(button1);
        panel.add(button2);
        springLayout.putConstraint(SpringLayout.WEST,button1,5,SpringLayout.WEST,panel);
        springLayout.putConstraint(SpringLayout.WEST,button2,5,SpringLayout.EAST,button1);
        setContentPane(panel);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}
