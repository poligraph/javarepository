package org.home.swing.myevent;

import java.util.EventListener;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 10.02.13
 * Time: 13:30
 * To change this template use File | Settings | File Templates.
 */
public interface HButtonPressListener extends EventListener {
    void hButtonPressed(HButtonPressEvent event);
}
