package org.home.swing.myevent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 10.02.13
 * Time: 13:32
 * To change this template use File | Settings | File Templates.
 */
public class HSimpleButton extends JComponent {
    private List listenerList=new ArrayList();
    private HButtonPressEvent event=new HButtonPressEvent(this);
    public HSimpleButton(){
        addMouseListener(new PressL());
        setPreferredSize(new Dimension(30,20));
        setSize(new Dimension(30,20));
    }
    public void addButtonPressListener(HButtonPressListener i){
        listenerList.add(i);
    }
    public void removeButtonPressListener(HButtonPressListener i){
        listenerList.remove(i);
    }
    @Override
    public void paintComponent(Graphics g){
        g.setColor(Color.green);
        g.fillRect(0,0,30,20);
        g.setColor(Color.black);
        g.draw3DRect(0,0,30,20,true);
    }
    protected void fireButtonPressed(){
        for(Object buttonPressListener:listenerList){
            ((HButtonPressListener)buttonPressListener).hButtonPressed(event);
        }
    }

    private class PressL extends MouseAdapter {
        @Override
        public void mousePressed(MouseEvent e){
            fireButtonPressed();
        }
    }
}
