<%--
  Created by IntelliJ IDEA.
  User: Hannibal
  Date: 09.03.12
  Time: 20:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="css/style.css" type="text/css"/>
</head>
<body>
<div id="container">
    <div id="header">Регистрация нового пациента</div>
    <div id="sidebar">
        <p><a href="index.jsp">На главную</a></p>
          <p><a href="doctorview.jsp">Врачи больницы</a>
        <p><a href="diahnos.jsp">Постановка предварительного диагноза</a>
         <p><a href="selection.jsp">Поиск пациентов</a></p>
        <a href="writedown.jsp">Выписать пациента</a>
    </div>
    <div id="content">
        <hr size="2px" >
        <form method="post" action="registration">
            <p>Имя пациента </p>
            <input type="text" name="pacient_name">
            <p>Фамилия пациента</p>
            <input type="text" name="pacient_surname">
            <p>Возраст</p>
            <input type="text" name="age">
            <p>Адрес</p>
            <input type="text" name="pacient_address">
            <p>Номер страхового полиса</p>
            <input type="text" name="policy_number">
            <hr size="2px">
            <input type="submit" value="Добавить">
        </form>
    </div>
    <div id="footer">Clinic patient record system</div>
</div>
</body>
</html>