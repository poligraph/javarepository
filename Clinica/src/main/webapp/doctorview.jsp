<%--
  Created by IntelliJ IDEA.
  User: Hannibal
  Date: 09.03.12
  Time: 20:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
<title></title>
<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body>
	<div id="container">
		<div id="header">Врачи больницы</div>
		<div id="sidebar">
			<p>
				<a href="index.jsp">На главную</a>
			<p>
				<a href="diahnos.jsp">Постановка предварительного диагноза</a>
			<p>
				<a href="registration.jsp">Регистрация пациента</a>
			</p>
			<p>
				<a href="selection.jsp">Поиск пациентов</a>
			</p>
			<p>
				<a href="writedown.jsp">Выписать пациента</a>
			</p>
		</div>
		<div id="content">
			<form action="doctors" method="post">
				<p>Специализация врача</p>
				<input type="text" name="special">
				<input type="submit" value="Поиск">
				<hr border="2px">
				<center>
					<table border="2px">
					${doctors}
					</table>
				</center>
			</form>
		</div>
		<div id="footer">Clinic patient record system</div>
	</div>
</body>
</html>