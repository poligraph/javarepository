<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<link rel="stylesheet" href="css/style.css" type="text/css" />

</head>
<body>
	<div id="container">
		<div id="header">Постановка предварительного диагноза</div>
		<div id="sidebar">
			<p>
				<a href="index.jsp">На главную</a>
			</p>
			<p>
				<a href="doctorview.jsp">Врачи больницы</a>
			<p>
				<a href="registration.jsp">Регистрация пациента</a>
			</p>
			<p>
				<a href="selection.jsp">Поиск пациентов</a>
			</p>
			<p>
				<a href="writedown.jsp">Выписать пациента</a>
		</div>
		<div id="content">
			<form method="post" action="#">
				<p>Симптомы сопровождающие болезнь</p>
				<input type="checkbox">Головная боль<br> <input
					type="checkbox">Боли в сердце<br> <input
					type="checkbox">Боль в желудке<br> <input
					type="checkbox">Высокая температура<br> <input
					type="checkbox">Озноб<br> <input type="checkbox">Боль
				в горле<br> <input type="submit" value="Возможные диагнозы">
			</form>
		</div>
		<div id="footer">Clinic patient record system</div>
	</div>
</body>
</html>