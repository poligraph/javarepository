package org.jazzteam;

import java.sql.*;

public class DatabaseConnector {
	protected String host = "jdbc:mysql://localhost/hospital";
	protected Connection connect;
	protected Statement statement;
	private String user = "root";
	private String password = "sql";

	public DatabaseConnector() throws ClassNotFoundException, SQLException {
		connect = null;
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		} catch (Exception e) {
			e.getMessage();
		}
		connect = DriverManager.getConnection(host, user, password);
	}

	public ResultSet query(String sql) throws SQLException {
		statement = connect.createStatement();
		return statement.executeQuery(sql);
	}

	public void insert(String sql) throws SQLException {
		statement = connect.createStatement();
		statement.executeUpdate(sql);
	}

	public void close() throws SQLException {
		connect.close();

	}

	public Connection getConnect() {
		return connect;
	}

	public void setConnect(Connection connect) {
		this.connect = connect;
	}
}
