package org.jazzteam;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddNewPacient extends HttpServlet {
	private DatabaseConnector db;

	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String lastname = request.getParameter("pacient_name");
		String firstname = request.getParameter("pacient_surname");
		String age = request.getParameter("age");
		String address = request.getParameter("pacient_address");
		String police = request.getParameter("policy_number");

		try {
			db = new DatabaseConnector();
			if (firstname.length() > 0 && lastname.length() > 0 && age.length()>0) {
				String sql = "INSERT INTO pacient VALUES (" + "null" + ",\""
						+ firstname + "\",\"" + lastname + "\",\"" + address
						+ "\"," + age + ");";
				db.insert(sql);
				db.close();
			}
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		RequestDispatcher Dispatcher = getServletContext()
				.getRequestDispatcher("/registration.jsp");
		response.sendRedirect("/Clinica/registration.jsp");

	}
}
