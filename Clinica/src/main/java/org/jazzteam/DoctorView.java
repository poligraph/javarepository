package org.jazzteam;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DoctorView extends HttpServlet {
	private static final String head = "<tr><td>�������</td><td>���</td><td>���������</td><td>����</td></tr>";
	private DatabaseConnector db;
	private String specialization;

	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			db = new DatabaseConnector();
			specialization = request.getParameter("special");
			String doctors = new String();
			ResultSet rs = db
					.query("SELECT * FROM doctors WHERE specialization=\""
							+ specialization + "\";");
			doctors += head;
			while (rs.next()) {
				doctors += "<tr><td>" + rs.getString("FirstName") + "<td>"
						+ rs.getString("Name") + "</td><td>"
						+ rs.getString("department") + "</td><td>"
						+ rs.getString("experience") + "</td>" + "</tr>";
				request.setAttribute("doctors", doctors);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		RequestDispatcher Dispatcher = getServletContext()
				.getRequestDispatcher("/doctorview.jsp");
		Dispatcher.forward(request, response);
	}
}
