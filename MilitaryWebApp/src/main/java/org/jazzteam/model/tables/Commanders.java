package org.jazzteam.model.tables;

/**
 * Created by IntelliJ IDEA.
 * User: Evgeniy
 * Date: 19.11.12
 * Time: 19:38
 * To change this template use File | Settings | File Templates.
 */
public class Commanders {
    private String fullNameOfficer;
    private String rank;
    private String address;
    private String phone;
    private String photo;
    private int birth;
    private String comments;

    public Commanders(){}

    public Commanders(String fullNameOfficer, String rank, String address, String phone,int birth) {
        setFullNameOfficer(fullNameOfficer);
        setRank(rank);
        setAddress(address);
        setPhone(phone);
        setBirth(birth);
    }
    public void setPhoto(String photo) {
        this.photo = photo;
    }
    public String getPhoto() {
        return photo;
    }

    public String getComments() {
        return comments;
    }
    public int getBirth() {
        return birth;
    }
    public void setBirth(int birth) {
        this.birth = birth;
    }

    public String getFullNameOfficer() {
        return fullNameOfficer;
    }
    public void setFullNameOfficer(String fullNameOfficer) {
        this.fullNameOfficer = fullNameOfficer;
    }
    public void setComments(String comments) {
        this.comments = comments;
    }
    public String getRank() {
        return rank;
    }
    public void setRank(String rank) {
        this.rank = rank;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

}
