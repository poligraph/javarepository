package org.jazzteam.model.tables;

/**
 * Created by IntelliJ IDEA.
 * User: Evgeniy
 * Date: 19.11.12
 * Time: 19:12
 * To change this template use File | Settings | File Templates.
 */
public class Battalion {
    private String numbers;
    private Commanders commander;
    private String battalion;
    public Battalion(){}
    public Battalion(String numbers,String battalion,Commanders commander) {
        setNumbers(numbers);
        setBattalion(battalion);
        setCommander(commander);
    }
    public Commanders getCommander() {
        return commander;
    }

    public void setCommander(Commanders commander) {
        this.commander = commander;
    }
    public String  getNumbers() {
        return numbers;
    }
    public void setNumbers(String numbers) {
        this.numbers = numbers;
    }
    public String getBattalion() {
        return battalion;
    }
    public void setBattalion(String battalion) {
        this.battalion = battalion;
    }

}
