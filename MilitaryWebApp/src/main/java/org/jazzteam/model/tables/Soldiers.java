package org.jazzteam.model.tables;

/**
 * Created by IntelliJ IDEA.
 * User: Evgeniy
 * Date: 19.11.12
 * Time: 19:19
 * To change this template use File | Settings | File Templates.
 */
public class Soldiers {
    public String getPlatoonName() {
        return platoonName;
    }

    public void setPlatoonName(String platoonName) {
        this.platoonName = platoonName;
    }

    private String platoonName;
    private String rank;
    private int kind;
    private String fullNameSoldier;
    private String start;
    private String end;
    private int idDepartment;
    private String passport;
    private String comment;
    private String photo;
    public Soldiers(){}
    public Soldiers(String fullNameSoldier,int idDepartment,String platoon,String rank,int kind,String passport,String start,String end){
        setRank(rank);
        setKind(kind);
        setFullNameSoldier(fullNameSoldier);
        setStart(start);
        setEnd(end);
        setIdDepartment(idDepartment);
        setPassport(passport);
        setPlatoonName(platoon);
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhoto() {
        return photo;
    }

    public int getKind() {
        return kind;
    }

    public void setKind(int kind) {
        this.kind = kind;
    }

    public int getIdDepartment() {
        return idDepartment;
    }

    public void setIdDepartment(int idDepartment) {
        this.idDepartment = idDepartment;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getComment() {
        return comment;
    }

    public void setComments(String comment) {
        this.comment = comment;
    }
    public String getRank() {
        return rank;
    }
    public void setRank(String rank) {
        this.rank = rank;
    }
    public String getFullNameSoldier() {
        return fullNameSoldier;
    }
    public void setFullNameSoldier(String fullNameSoldier) {
        this.fullNameSoldier = fullNameSoldier;
    }
    public String getStart() {
        return start;
    }
    public void setStart(String start) {
        this.start = start;
    }
    public String getEnd() {
        return end;
    }
    public void setEnd(String end) {
        this.end = end;
    }

}
