package org.jazzteam.model.tables;

/**
 * Created by IntelliJ IDEA.
 * User: Evgeniy
 * Date: 19.11.12
 * Time: 19:17
 * To change this template use File | Settings | File Templates.
 */
public class Platoon {
    private Company company;
    private Commanders commander;
    private String platoonName;

    public Platoon(){}

    public Platoon(String platoonName,Commanders commander,Company company) {
        setPlatoonName(platoonName);
        setCommander(commander);
        setCompany(company);
    }

    public Company getCompany() {
        return company;
    }
    public Commanders getCommander() {
        return commander;
    }
    public void setCompany(Company company) {
        this.company = company;
    }
    public void setCommander(Commanders commander) {
        this.commander = commander;
    }
    public String getPlatoonName() {
        return platoonName;
    }
    public void setPlatoonName(String platoonName) {
        this.platoonName = platoonName;
    }

}
