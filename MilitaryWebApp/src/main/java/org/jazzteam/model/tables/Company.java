package org.jazzteam.model.tables;

/**
 * Created by IntelliJ IDEA.
 * User: Evgeniy
 * Date: 19.11.12
 * Time: 19:35
 * To change this template use File | Settings | File Templates.
 */
public class Company {
    private Battalion battalion;

    private Commanders commander;

    private String companyName;

    public Company(){}

    public Company(String CompanyName,Battalion battalion,Commanders commander) {
        setCompanyName(CompanyName);
        setBattalion(battalion);
        setCommander(commander);

    }
    public Battalion getBattalion() {
        return battalion;
    }
    public void setBattalion(Battalion battalion) {
        this.battalion = battalion;
    }
    public String getCompanyName() {
        return companyName;
    }
    public Commanders getCommander() {
        return commander;
    }
    public void setCompanyName(String name) {
        this.companyName = name;
    }
    public void setCommander(Commanders commander) {
        this.commander = commander;
    }

}
