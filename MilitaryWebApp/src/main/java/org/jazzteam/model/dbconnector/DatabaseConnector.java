package org.jazzteam.model.dbconnector;

import java.sql.*;

/**
 * Created by IntelliJ IDEA.
 * User: Evgeniy
 * Date: 19.11.12
 * Time: 19:48
 * To change this template use File | Settings | File Templates.
 */
public class DatabaseConnector {
    private Connection connection=null;
    private String databaseName;
    private final String host="jdbc:mysql://localhost:3306/";
    public DatabaseConnector(String user,String password,String databaseName) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        this.databaseName=databaseName;
        try{
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connection= DriverManager.getConnection(host + this.databaseName, user, password);

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    public CallableStatement getCallableStatement(String procedureCall) throws SQLException {
        return connection.prepareCall(procedureCall);
    }
    public PreparedStatement getPrepareStatement(String procedureCall) throws SQLException {
        return connection.prepareStatement(procedureCall);
    }
}
