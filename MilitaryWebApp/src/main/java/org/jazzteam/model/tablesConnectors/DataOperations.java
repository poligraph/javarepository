package org.jazzteam.model.tablesConnectors;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Evgeniy
 * Date: 19.11.12
 * Time: 19:43
 * To change this template use File | Settings | File Templates.
 */
public interface DataOperations<E> {
    public void insert(E value) throws SQLException;
    public void select() throws SQLException;
    void update(E oldValue,E newValue) throws SQLException;
    public void remove(E value) throws SQLException;
    public List<E> search(E value);
    public List<E> getList();
}
