package org.jazzteam.model.tablesConnectors;

import org.jazzteam.model.dbconnector.DatabaseConnector;
import org.jazzteam.model.tables.Platoon;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: evgenii
 * Date: 07.12.12
 * Time: 18:18
 * To change this template use File | Settings | File Templates.
 */
public class PlatoonConnector implements DataOperations {
    private static PlatoonConnector platoonConnector;
    private static final String SELECT_PROCEDURE="SelectAllPlatoon";
    private DatabaseConnector databaseConnector;
    private CallableStatement selectCallableStatement;
    private List<Platoon> platoons;
    private PlatoonConnector(){}
    private PlatoonConnector(DatabaseConnector databaseConnector) throws SQLException {
        this.databaseConnector=databaseConnector;
        this.platoons=new ArrayList<Platoon>();
        this.selectCallableStatement=databaseConnector.getCallableStatement("CALL "+SELECT_PROCEDURE+"()");

    }
    @Override
    public void insert(Object value) throws SQLException {
    }

    @Override
    public void select() throws SQLException {
        ResultSet resultSet=selectCallableStatement.executeQuery();
        while(resultSet.next()){
            Platoon platoon=new Platoon();
            platoon.setPlatoonName(resultSet.getString("platoonName"));
            platoons.add(platoon);
        }
    }

    @Override
    public void update(Object oldValue, Object newValue) throws SQLException {
    }

    @Override
    public void remove(Object value) throws SQLException {
    }

    @Override
    public List search(Object value) {
        return null;
    }

    @Override
    public List<Platoon> getList() {
        return platoons;
    }
    public static PlatoonConnector getInstance(DatabaseConnector databaseConnector) throws SQLException {
        if(platoonConnector==null){
            platoonConnector=new PlatoonConnector(databaseConnector);
            platoonConnector.select();
        }
        return platoonConnector;
    }
}
