package org.jazzteam.model.tablesConnectors;

import org.jazzteam.model.dbconnector.DatabaseConnector;
import org.jazzteam.model.tables.Battalion;
import org.jazzteam.model.tables.Commanders;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Evgeniy
 * Date: 24.11.12
 * Time: 16:38
 * To change this template use File | Settings | File Templates.
 */
public class BattalionConnector implements DataOperations<Battalion>{
    private static BattalionConnector battalionConnector=null;
    private static final String SELECT_PROCEDURE="SelectAllBattalions";
    private List<Battalion> battalions;
    private CallableStatement selectCallableStatement;
    private  BattalionConnector(DatabaseConnector databaseConnector) throws SQLException {
        selectCallableStatement=databaseConnector.getCallableStatement("CALL "+SELECT_PROCEDURE+"()");
        battalions=new ArrayList<Battalion>();
    }
    private BattalionConnector(){}
    @Override
    public void insert(Battalion value) throws SQLException {
    }

    @Override
    public void select() throws SQLException {
        ResultSet resultSet=selectCallableStatement.executeQuery();
        while(resultSet.next()){
            Battalion battalion=new Battalion();
            battalion.setNumbers(resultSet.getString("numbers"));
            battalion.setBattalion(resultSet.getString("battalion"));
            Commanders commander=new Commanders();
            commander.setFullNameOfficer(resultSet.getString("fullNameOfficer"));
            commander.setRank(resultSet.getString("rank"));
            commander.setAddress(resultSet.getString("address"));
            commander.setPhone(resultSet.getString("phone"));
            commander.setPhoto(resultSet.getString("photo"));
            commander.setBirth(resultSet.getInt("birth"));
            commander.setComments(resultSet.getString("comments"));
            battalion.setCommander(commander);
            battalions.add(battalion);
        }
    }

    @Override
    public void update(Battalion oldValue, Battalion newValue) throws SQLException {

    }

    @Override
    public void remove(Battalion value) throws SQLException {
    }

    @Override
    public List<Battalion> search(Battalion value) {
        return null;
    }

    @Override
    public List<Battalion> getList() {
        return battalions;
    }
    public static DataOperations<Battalion> getInstance(DatabaseConnector databaseConnector) throws SQLException {
        if(battalionConnector==null){
            battalionConnector=new BattalionConnector(databaseConnector);
            battalionConnector.select();
        }
        return battalionConnector;
    }
}
