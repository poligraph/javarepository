package org.jazzteam.model.tablesConnectors;

import org.jazzteam.model.dbconnector.DatabaseConnector;
import org.jazzteam.model.tables.Commanders;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Evgeniy
 * Date: 24.11.12
 * Time: 17:00
 * To change this template use File | Settings | File Templates.
 */
public class CommandersConnector implements DataOperations<Commanders> {
    private static CommandersConnector commandersConnector = null;
    private static final String SELECT_PROCEDURE = "selectAllCommanders";
    private static final String REMOVE_PROCEDURE = "deleteCommander";
    private static final String INSERT_PROCEDURE = "addCommander";
    private CallableStatement selectCallableStatement;
    private CallableStatement removeCallableStatement;
    private CallableStatement insertCallableStatement;
    private PreparedStatement updatePreparedStatement;
    List<Commanders> list;

    private CommandersConnector(DatabaseConnector databaseConnector) throws SQLException {
        list = new ArrayList<Commanders>();
        selectCallableStatement = databaseConnector.getCallableStatement("CALL " + SELECT_PROCEDURE + "()");
        removeCallableStatement = databaseConnector.getCallableStatement("CALL " + REMOVE_PROCEDURE + "(?)");
        insertCallableStatement = databaseConnector.getCallableStatement("CALL " + INSERT_PROCEDURE + "(?,?,?,?,?,?,?)");
        updatePreparedStatement = databaseConnector.getPrepareStatement("UPDATE commanders SET fullNameOfficer=?,rank=?,address=?," +
                "phone=?,birth=?,photo=?,comments=? WHERE fullNameOfficer=? LIMIT 1");
    }

    private CommandersConnector() {
    }

    @Override
    public void insert(Commanders value) throws SQLException {
        insertCallableStatement.setString(1, value.getFullNameOfficer());
        insertCallableStatement.setString(2, value.getRank());
        insertCallableStatement.setString(3, value.getAddress());
        insertCallableStatement.setString(4, value.getPhone());
        insertCallableStatement.setInt(5, value.getBirth());
        insertCallableStatement.setString(6, value.getPhoto());
        insertCallableStatement.setString(7, value.getComments());
        insertCallableStatement.execute();
        list.add(value);
    }

    @Override
    public void select() throws SQLException {
        ResultSet resultSet = selectCallableStatement.executeQuery();
        while (resultSet.next()) {
            Commanders commanders = new Commanders();
            commanders.setAddress(resultSet.getString("address"));
            commanders.setBirth(resultSet.getInt("birth"));
            commanders.setComments(resultSet.getString("comments"));
            commanders.setPhoto(resultSet.getString("photo"));
            commanders.setFullNameOfficer(resultSet.getString("fullNameOfficer"));
            commanders.setRank(resultSet.getString("rank"));
            commanders.setPhone(resultSet.getString("phone"));
            list.add(commanders);
        }
    }

    @Override
    public void update(Commanders oldValue, Commanders newValue) throws SQLException {
        updatePreparedStatement.setString(1, newValue.getFullNameOfficer());
        updatePreparedStatement.setString(2, newValue.getRank());
        updatePreparedStatement.setString(3, newValue.getAddress());
        updatePreparedStatement.setString(4, newValue.getPhone());
        updatePreparedStatement.setInt(5, newValue.getBirth());
        updatePreparedStatement.setString(6, newValue.getPhoto());
        updatePreparedStatement.setString(7, newValue.getComments());
        updatePreparedStatement.setString(8, oldValue.getFullNameOfficer());
        updatePreparedStatement.execute();
        list.remove(oldValue);
        list.add(newValue);
    }

    @Override
    public void remove(Commanders value) throws SQLException {
        removeCallableStatement.setString(1, value.getFullNameOfficer());
        removeCallableStatement.execute();
        list.remove(value);
    }

    @Override
    public List<Commanders> search(Commanders value) {
        List<Commanders> resCommanders = new ArrayList<Commanders>();
        for (Commanders commander : list) {
            if (commander.getRank().equalsIgnoreCase(value.getRank()) || commander.getFullNameOfficer().equalsIgnoreCase(value.getFullNameOfficer()) ||
                    commander.getAddress().equalsIgnoreCase(value.getAddress()) || commander.getBirth() == value.getBirth() || commander.getPhone().equalsIgnoreCase(value.getPhone())) {
                resCommanders.add(commander);
            }
        }
        return resCommanders;
    }

    public Commanders getByName(String name) {
        for (Commanders commander : list) {
            if (commander.getFullNameOfficer().equalsIgnoreCase(name)) {
                return commander;
            }
        }
        return null;
    }

    @Override
    public List<Commanders> getList() {
        return list;
    }

    public static DataOperations getInstance(DatabaseConnector databaseConnector) throws SQLException {
        if (commandersConnector == null) {
            commandersConnector = new CommandersConnector(databaseConnector);
            commandersConnector.select();
        }
        return commandersConnector;
    }
}
