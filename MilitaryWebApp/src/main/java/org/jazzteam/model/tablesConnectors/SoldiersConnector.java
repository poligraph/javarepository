package org.jazzteam.model.tablesConnectors;

import org.jazzteam.model.dbconnector.DatabaseConnector;
import org.jazzteam.model.tables.Soldiers;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 27.11.12
 * Time: 21:45
 * To change this template use File | Settings | File Templates.
 */
public class SoldiersConnector implements DataOperations<Soldiers>{
    private static SoldiersConnector soldiersConnector=null;
    private CallableStatement selectCallableStatement;
    private CallableStatement insertCallableStatement;
    private CallableStatement removeCallableStatement;
    private PreparedStatement updatePreparedStatement;
    private static final String SELECT_PROCEDURE="selectAllSoldiers";
    private static final String INSERT_PROCEDURE="addSoldier";
    private static final String REMOVE_PROCEDURE="deleteSoldier";
    private List<Soldiers> list;
    private SoldiersConnector(DatabaseConnector databaseConnector) throws SQLException {
        selectCallableStatement=databaseConnector.getCallableStatement("CALL "+SELECT_PROCEDURE+"()");
        insertCallableStatement=databaseConnector.getCallableStatement("CALL "+INSERT_PROCEDURE+"(?,?,?,?,?,?,?,?,?,?)");
        updatePreparedStatement=databaseConnector.getPrepareStatement("UPDATE soldiers SET fullNameSoldier=?,idDepartment=?," +
                "rank=?,kind=?,start=?,stop=?,passport=?,comments=?,photo=? WHERE fullNameSoldier=? LIMIT 1");
        removeCallableStatement=databaseConnector.getCallableStatement("CALL "+REMOVE_PROCEDURE+"(?)");
        list=new ArrayList<Soldiers>();
    }
    private SoldiersConnector(){}

    public static DataOperations getInstance(DatabaseConnector databaseConnector) throws SQLException {
        if(soldiersConnector==null){
            soldiersConnector=new SoldiersConnector(databaseConnector);
            soldiersConnector.select();
        }
        return soldiersConnector;
    }
    @Override
    public void insert(Soldiers value) throws SQLException {
        insertCallableStatement.setString(1,value.getFullNameSoldier());
        insertCallableStatement.setInt(2,value.getIdDepartment());
        insertCallableStatement.setString(3,value.getRank());
        insertCallableStatement.setInt(4,value.getKind());
        insertCallableStatement.setString(5,value.getStart());
        insertCallableStatement.setString(6,value.getEnd());
        insertCallableStatement.setString(7,value.getPlatoonName());
        insertCallableStatement.setString(8,value.getPassport());
        insertCallableStatement.setString(9,value.getComment());
        insertCallableStatement.setString(10,value.getPhoto());
        insertCallableStatement.execute();
        list.add(value);
    }

    @Override
    public void select() throws SQLException {
        ResultSet resultSet=selectCallableStatement.executeQuery();
        while(resultSet.next()){
            Soldiers soldiers=new Soldiers();
            soldiers.setComments(resultSet.getString("comments"));
            soldiers.setEnd(resultSet.getString("stop"));
            soldiers.setFullNameSoldier(resultSet.getString("fullNameSoldier"));
            soldiers.setIdDepartment(resultSet.getInt("idDepartment"));
            soldiers.setKind(resultSet.getInt("kind"));
            soldiers.setPassport(resultSet.getString("passport"));
            soldiers.setStart(resultSet.getString("start"));
            soldiers.setRank(resultSet.getString("rank"));
            soldiers.setPhoto(resultSet.getString("photo"));
            soldiers.setPlatoonName(resultSet.getString("platoonName"));
            list.add(soldiers);
        }
    }

    @Override
    public void update(Soldiers oldValue, Soldiers newValue) throws SQLException {
        updatePreparedStatement.setString(1,newValue.getFullNameSoldier());
        updatePreparedStatement.setInt(2, newValue.getIdDepartment());
        updatePreparedStatement.setString(3,newValue.getRank());
        updatePreparedStatement.setInt(4, newValue.getKind());
        updatePreparedStatement.setString(5,newValue.getStart());
        updatePreparedStatement.setString(6,newValue.getEnd());
        updatePreparedStatement.setString(7,newValue.getPassport());
        updatePreparedStatement.setString(8,newValue.getComment());
        updatePreparedStatement.setString(9,newValue.getPhoto());
        updatePreparedStatement.setString(10,oldValue.getFullNameSoldier());
        updatePreparedStatement.execute();
        list.remove(oldValue);
        list.add(newValue);
    }

    @Override
    public void remove(Soldiers value) throws SQLException {
        removeCallableStatement.setString(1,value.getFullNameSoldier());
        removeCallableStatement.execute();
        list.remove(value);
    }

    @Override
    public List<Soldiers> search(Soldiers value) {
        List<Soldiers> resultList=new ArrayList<Soldiers>();
        for(Soldiers soldier:list){
            if(soldier.getFullNameSoldier().equalsIgnoreCase(value.getFullNameSoldier())||
                    soldier.getPassport().equalsIgnoreCase(value.getPassport())||
                    soldier.getIdDepartment()==value.getIdDepartment()||
                    soldier.getRank().equalsIgnoreCase(value.getRank())){
                resultList.add(soldier);
            }
        }
        return resultList;
    }

    @Override
    public List<Soldiers> getList() {
        return list;
    }
    public Soldiers getByName(String name){
        for(Soldiers soldiers:list){
            if (soldiers.getFullNameSoldier().equalsIgnoreCase(name)){
                return soldiers;
            }
        }
        return list.get(0);
    }
}
