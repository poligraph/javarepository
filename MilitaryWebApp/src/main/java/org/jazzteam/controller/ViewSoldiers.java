package org.jazzteam.controller;

import org.jazzteam.model.dbconnector.DatabaseConnector;
import org.jazzteam.model.tables.Battalion;
import org.jazzteam.model.tables.Soldiers;
import org.jazzteam.model.tablesConnectors.BattalionConnector;
import org.jazzteam.model.tablesConnectors.DataOperations;
import org.jazzteam.model.tablesConnectors.SoldiersConnector;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 30.11.12
 * Time: 10:26
 * To change this template use File | Settings | File Templates.
 */
public class ViewSoldiers extends HttpServlet {
    private DataOperations<Soldiers> soldiersConnector;
    private DatabaseConnector databaseConnector;
    private DataOperations<Battalion> battalionDataOperations;
    @Override
    public void init(){
        try {
            databaseConnector=new DatabaseConnector("root","root","military");
            soldiersConnector=SoldiersConnector.getInstance(databaseConnector);
            battalionDataOperations= BattalionConnector.getInstance(databaseConnector);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IllegalAccessException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (InstantiationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        JSONObject jsonObject=new JSONObject();
        String tableBody="";
        String tableHead="Фамилия Имя Отчество!Звание!Имя взвода";
        for(Soldiers soldier:soldiersConnector.getList()){
            tableBody+=soldier.getFullNameSoldier()+"!";
            tableBody+=soldier.getRank()+"!";
            tableBody+=soldier.getPlatoonName()+"!";
        }
        try {
            jsonObject.put("tbody",tableBody );
            jsonObject.put("thead",tableHead);
            jsonObject.put("number",battalionDataOperations.getList().get(0).getNumbers());
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        response.getWriter().print(jsonObject.toString());

    }
}
