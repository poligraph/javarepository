package org.jazzteam.controller;

import jxl.write.WriteException;
import org.jazzteam.model.dbconnector.DatabaseConnector;
import org.jazzteam.model.excel.ExcelExporter;
import org.jazzteam.model.tables.Battalion;
import org.jazzteam.model.tables.Commanders;
import org.jazzteam.model.tables.Soldiers;
import org.jazzteam.model.tablesConnectors.BattalionConnector;
import org.jazzteam.model.tablesConnectors.CommandersConnector;
import org.jazzteam.model.tablesConnectors.DataOperations;
import org.jazzteam.model.tablesConnectors.SoldiersConnector;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 08.12.12
 * Time: 16:54
 * To change this template use File | Settings | File Templates.
 */
public class ExcelExport extends HttpServlet {
    private DatabaseConnector databaseConnector;
    private DataOperations<Commanders> commandersDataOperations;
    private DataOperations<Soldiers> soldiersDataOperations;
    private DataOperations<Battalion> battalionDataOperations;
    private ExcelExporter excelExporter;

    @Override
    public void init() {
        try {
            databaseConnector = new DatabaseConnector("root", "root", "military");
            commandersDataOperations = CommandersConnector.getInstance(databaseConnector);
            soldiersDataOperations = SoldiersConnector.getInstance(databaseConnector);
            battalionDataOperations = BattalionConnector.getInstance(databaseConnector);
            excelExporter = new ExcelExporter("report.xls");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IllegalAccessException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (InstantiationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            insertToExel();
        } catch (WriteException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition",
                "attachment;filename=report.xls");
        InputStream inputStream = new FileInputStream(new File("report.xls"));
        ServletOutputStream outputStream = response.getOutputStream();
        byte[] outputByte = new byte[4096];
        while (inputStream.read(outputByte, 0, 4096) != -1) {
            outputStream.write(outputByte, 0, 4096);
        }
        inputStream.close();
        outputStream.flush();
        outputStream.close();
    }

    public void insertToExel() throws WriteException, IOException {
        excelExporter.createSheet("Офицеры");
        excelExporter.writeInExcel(0, 0, "ФИО", "Офицеры");
        excelExporter.writeInExcel(0, 1, "Звание", "Офицеры");
        excelExporter.writeInExcel(0, 2, "Адрес", "Офицеры");
        excelExporter.writeInExcel(0, 3, "Телефон", "Офицеры");
        excelExporter.writeInExcel(0, 4, "Год рождения", "Офицеры");
        excelExporter.writeInExcel(0, 5, "Примечания", "Офицеры");
        for (int i = 0, row = 1; i < commandersDataOperations.getList().size(); i++) {
            Commanders commanders = commandersDataOperations.getList().get(i);
            excelExporter.writeInExcel(row, 0, commanders.getFullNameOfficer(), "Офицеры");
            excelExporter.writeInExcel(row, 1, commanders.getRank(), "Офицеры");
            excelExporter.writeInExcel(row, 2, commanders.getAddress(), "Офицеры");
            excelExporter.writeInExcel(row, 3, commanders.getPhone(), "Офицеры");
            excelExporter.writeInExcel(row, 4, commanders.getBirth() + "", "Офицеры");
            excelExporter.writeInExcel(row, 5, commanders.getComments() + "", "Офицеры");
            row++;
        }
        excelExporter.createSheet("Солдаты");
        excelExporter.writeInExcel(0, 0, "ФИО", "Солдаты");
        excelExporter.writeInExcel(0, 1, "Номер отделения", "Солдаты");
        excelExporter.writeInExcel(0, 2, "Звание", "Солдаты");
        excelExporter.writeInExcel(0, 3, "Начало службы", "Солдаты");
        excelExporter.writeInExcel(0, 4, "Название взвода", "Солдаты");
        excelExporter.writeInExcel(0, 5, "Номер билета", "Солдаты");
        excelExporter.writeInExcel(0, 6, "Примечания", "Солдаты");
        for (int i = 0, row = 1; i < soldiersDataOperations.getList().size(); i++) {
            Soldiers soldiers = soldiersDataOperations.getList().get(i);
            excelExporter.writeInExcel(row, 0, soldiers.getFullNameSoldier(), "Солдаты");
            excelExporter.writeInExcel(row, 1, soldiers.getIdDepartment() + "", "Солдаты");
            excelExporter.writeInExcel(row, 2, soldiers.getRank(), "Солдаты");
            excelExporter.writeInExcel(row, 3, soldiers.getStart(), "Солдаты");
            excelExporter.writeInExcel(row, 4, soldiers.getPlatoonName(), "Солдаты");
            excelExporter.writeInExcel(row, 5, soldiers.getPassport(), "Солдаты");
            excelExporter.writeInExcel(row, 6, soldiers.getComment(), "Солдаты");
            row++;
        }
        excelExporter.createSheet("Батальоны");
        excelExporter.writeInExcel(0, 0, "Номер части", "Батальоны");
        excelExporter.writeInExcel(0, 1, "Название батальона", "Батальоны");
        excelExporter.writeInExcel(0, 2, "Командир батальона", "Батальоны");
        for (int i = 0, row = 1; i < battalionDataOperations.getList().size(); i++) {
            Battalion battalion = battalionDataOperations.getList().get(i);
            excelExporter.writeInExcel(row, 0, battalion.getNumbers(), "Батальоны");
            excelExporter.writeInExcel(row, 1, battalion.getBattalion(), "Батальоны");
            excelExporter.writeInExcel(row, 2, battalion.getCommander().getFullNameOfficer(), "Батальоны");
            row++;
        }
        excelExporter.closeExcel();
    }
}
