package org.jazzteam.controller;

import org.jazzteam.model.dbconnector.DatabaseConnector;
import org.jazzteam.model.tables.Commanders;
import org.jazzteam.model.tablesConnectors.CommandersConnector;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 30.11.12
 * Time: 17:54
 * To change this template use File | Settings | File Templates.
 */
public class ShowOfficerRecord extends HttpServlet {
    private DatabaseConnector databaseConnector;
    private CommandersConnector commandersDataOperations;
    @Override
    public void init(){
        try {
            databaseConnector=new DatabaseConnector("root","root","military");
            commandersDataOperations= (CommandersConnector) CommandersConnector.getInstance(databaseConnector);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IllegalAccessException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (InstantiationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        JSONObject jsonObject=new JSONObject();
        String officerName=request.getParameter("officerName");
        Commanders commanders=new Commanders();
        commanders.setFullNameOfficer(officerName);
        commanders=commandersDataOperations.search(commanders).get(0);
        try {
            jsonObject.put("fullName",commanders.getFullNameOfficer());
            jsonObject.put("birth",commanders.getBirth());
            jsonObject.put("address",commanders.getAddress());
            jsonObject.put("comments",commanders.getComments());
            jsonObject.put("phone",commanders.getPhone());
            jsonObject.put("rank",commanders.getRank());
            jsonObject.put("photo",commanders.getPhoto());
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        response.getWriter().print(jsonObject.toString());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Commanders commanders=commandersDataOperations.getByName(request.getParameter("fullName"));
        try {
            if(request.getParameter("action").equalsIgnoreCase("change")){
                Commanders newCommanderRecord=new Commanders();
                newCommanderRecord.setFullNameOfficer(request.getParameter("newFullName"));
                newCommanderRecord.setRank(request.getParameter("newRank"));
                newCommanderRecord.setPhone(request.getParameter("newPhone"));
                newCommanderRecord.setAddress(request.getParameter("newAddress"));
                newCommanderRecord.setBirth(Integer.parseInt(request.getParameter("newBirth")));
                newCommanderRecord.setComments(request.getParameter("newComments"));
                newCommanderRecord.setPhoto(request.getParameter("newPhoto"));
            commandersDataOperations.update(commanders,newCommanderRecord);
            }
            else if(request.getParameter("action").equalsIgnoreCase("remove")){
                commandersDataOperations.remove(commanders);
            }
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
