package org.jazzteam.controller;

import org.jazzteam.model.dbconnector.DatabaseConnector;
import org.jazzteam.model.tables.Commanders;
import org.jazzteam.model.tablesConnectors.CommandersConnector;
import org.jazzteam.model.tablesConnectors.DataOperations;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 03.12.12
 * Time: 16:46
 * To change this template use File | Settings | File Templates.
 */
public class AddOfficer extends HttpServlet {
    private DatabaseConnector databaseConnector;
    private DataOperations<Commanders> commandersDataOperations;

    @Override
    public void init() {
        try {
            databaseConnector = new DatabaseConnector("root", "root", "military");
            commandersDataOperations = CommandersConnector.getInstance(databaseConnector);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IllegalAccessException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (InstantiationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Commanders commanders = new Commanders();
        commanders.setFullNameOfficer(request.getParameter("fullName"));
        commanders.setRank(request.getParameter("rank"));
        commanders.setBirth(Integer.parseInt(request.getParameter("birth")));
        commanders.setAddress(request.getParameter("address"));
        commanders.setPhone(request.getParameter("phone"));
        commanders.setPhoto(request.getParameter("photo"));
        commanders.setComments(request.getParameter("comments"));
        try {
            commandersDataOperations.insert(commanders);
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
