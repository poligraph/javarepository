package org.jazzteam.controller;

import org.jazzteam.model.dbconnector.DatabaseConnector;
import org.jazzteam.model.tables.Soldiers;
import org.jazzteam.model.tablesConnectors.SoldiersConnector;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 01.12.12
 * Time: 10:08
 * To change this template use File | Settings | File Templates.
 */
public class ShowSoldierRecord extends HttpServlet {
    private DatabaseConnector databaseConnector;
    private SoldiersConnector soldiersDataOperations;
    @Override
    public void init(){
        try {
            databaseConnector=new DatabaseConnector("root","root","military");
            soldiersDataOperations= (SoldiersConnector) SoldiersConnector.getInstance(databaseConnector);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IllegalAccessException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (InstantiationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String fullNameSoldier=request.getParameter("fullNameSoldier");
        Soldiers soldiers=new Soldiers();
        soldiers.setFullNameSoldier(fullNameSoldier);
        JSONObject jsonObject=new JSONObject();
        soldiers=soldiersDataOperations.search(soldiers).get(0);
        try {
            jsonObject.put("platoonName",soldiers.getPlatoonName());
            jsonObject.put("rank",soldiers.getRank());
            jsonObject.put("fullName",soldiers.getFullNameSoldier());
            jsonObject.put("comment",soldiers.getComment());
            jsonObject.put("end",soldiers.getEnd());
            jsonObject.put("start",soldiers.getStart());
            jsonObject.put("photo",soldiers.getPhoto());
            if(soldiers.getKind()==0){
            jsonObject.put("kind","Срочная");
            }
            else{
                jsonObject.put("kind","Контрактная");
            }
            jsonObject.put("passport",soldiers.getPassport());
            jsonObject.put("department",soldiers.getIdDepartment());
            System.out.println(jsonObject.toString());
            response.getWriter().print(jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        Soldiers soldiers=soldiersDataOperations.getByName(request.getParameter("fullName"));
        Soldiers newSoldiers=new Soldiers();
            try {
                if(request.getParameter("action").equalsIgnoreCase("change")){
                newSoldiers.setFullNameSoldier(request.getParameter("newFullName"));
                newSoldiers.setStart(request.getParameter("newStart"));
                newSoldiers.setComments(request.getParameter("newComments"));
                newSoldiers.setEnd(request.getParameter("newStart"));
                newSoldiers.setIdDepartment(Integer.parseInt(request.getParameter("newDepartment")));
                newSoldiers.setKind(Integer.parseInt(request.getParameter("newKind")));
                newSoldiers.setPassport(request.getParameter("newPassport"));
                newSoldiers.setPhoto(request.getParameter("newPhoto"));
                newSoldiers.setRank(request.getParameter("newRank"));
                soldiersDataOperations.update(soldiers,newSoldiers);
                }
                else if(request.getParameter("action").equalsIgnoreCase("remove")){
                soldiersDataOperations.remove(soldiers);
                }
            } catch (SQLException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
    }
}
