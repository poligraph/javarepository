package org.jazzteam.controller;

import org.jazzteam.model.dbconnector.DatabaseConnector;
import org.jazzteam.model.tables.Commanders;
import org.jazzteam.model.tables.Soldiers;
import org.jazzteam.model.tablesConnectors.CommandersConnector;
import org.jazzteam.model.tablesConnectors.DataOperations;
import org.jazzteam.model.tablesConnectors.SoldiersConnector;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 08.12.12
 * Time: 14:11
 * To change this template use File | Settings | File Templates.
 */
public class Search extends HttpServlet {
    private DatabaseConnector databaseConnector;
    private DataOperations<Soldiers> soldiersDataOperations;
    private DataOperations<Commanders> commandersDataOperations;
    @Override
    public void init(){
        try {
            databaseConnector=new DatabaseConnector("root","root","military");
            soldiersDataOperations= SoldiersConnector.getInstance(databaseConnector);
            commandersDataOperations= CommandersConnector.getInstance(databaseConnector);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IllegalAccessException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (InstantiationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        JSONObject jsonObject=new JSONObject();
        Soldiers soldiers=new Soldiers();
        soldiers.setFullNameSoldier(request.getParameter("search"));
        soldiers.setRank(request.getParameter("search"));
        String soldiersTableBody="";
        for(Soldiers soldier:soldiersDataOperations.search(soldiers)){
            soldiersTableBody+="<td>"+soldier.getFullNameSoldier()+"</td>";
            soldiersTableBody+="<td>"+soldier.getRank()+"</td>";
            soldiersTableBody+="<td>"+soldier.getPlatoonName()+"</td>";
        }
        Commanders commanders=new Commanders();
        commanders.setFullNameOfficer(request.getParameter("search"));
        commanders.setRank(request.getParameter("search"));
        String officersTableBody="";
        for(Commanders commander:commandersDataOperations.search(commanders)){
            officersTableBody+="<td>"+commander.getFullNameOfficer()+"</td>";
            officersTableBody+="<td>"+commander.getRank()+"</td>";
            officersTableBody+="<td>"+commander.getBirth()+"</td>";
        }
        try {
            jsonObject.put("soldiersTableBody",soldiersTableBody);
            jsonObject.put("officersTableBody",officersTableBody);
            jsonObject.put("officersTableHead","Фамилия Имя Отчество!Звание!Год рождения");
            jsonObject.put("soldiersTableHead","Фамилия Имя Отчество!Звание!Имя взвода");
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        response.getWriter().println(jsonObject.toString());
    }
}
