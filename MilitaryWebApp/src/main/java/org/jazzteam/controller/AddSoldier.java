package org.jazzteam.controller;

import org.jazzteam.model.tablesConnectors.PlatoonConnector;
import org.jazzteam.model.dbconnector.DatabaseConnector;
import org.jazzteam.model.tables.Platoon;
import org.jazzteam.model.tables.Soldiers;
import org.jazzteam.model.tablesConnectors.DataOperations;
import org.jazzteam.model.tablesConnectors.SoldiersConnector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: evgenii
 * Date: 07.12.12
 * Time: 17:32
 * To change this template use File | Settings | File Templates.
 */
public class AddSoldier extends HttpServlet {
    private DatabaseConnector databaseConnector;
    private DataOperations soldiersDataOperations;
    private DataOperations<Platoon> platoonDataOperations;
    @Override
    public void init(){
        try {
            databaseConnector=new DatabaseConnector("root","root","military");
            soldiersDataOperations= SoldiersConnector.getInstance(databaseConnector);
            platoonDataOperations=PlatoonConnector.getInstance(databaseConnector);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IllegalAccessException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (InstantiationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String result="";
        for(Platoon platoon:platoonDataOperations.getList()){
            result+=platoon.getPlatoonName()+"!";
            System.out.println(result);
        }
        response.getWriter().println(result);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        Soldiers soldiers=new Soldiers();
        soldiers.setFullNameSoldier(request.getParameter("fullName"));
        soldiers.setStart(request.getParameter("start"));
        soldiers.setComments(request.getParameter("comments"));
        soldiers.setEnd(request.getParameter("start"));
        soldiers.setIdDepartment(Integer.parseInt(request.getParameter("department")));
        soldiers.setKind(Integer.parseInt(request.getParameter("kind")));
        soldiers.setPassport(request.getParameter("passport"));
        soldiers.setPhoto(request.getParameter("photo"));
        soldiers.setPlatoonName(request.getParameter("platoon"));
        soldiers.setRank(request.getParameter("rank"));
        try {
            soldiersDataOperations.insert(soldiers);
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
