package org.jazzteam.controller;

import org.jazzteam.model.dbconnector.DatabaseConnector;
import org.jazzteam.model.tables.Battalion;
import org.jazzteam.model.tables.Commanders;
import org.jazzteam.model.tablesConnectors.BattalionConnector;
import org.jazzteam.model.tablesConnectors.CommandersConnector;
import org.jazzteam.model.tablesConnectors.DataOperations;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 29.11.12
 * Time: 15:43
 * To change this template use File | Settings | File Templates.
 */
public class ViewOfficers extends HttpServlet {
    private DatabaseConnector databaseConnector;
    private DataOperations<Commanders> commandersDataOperations;
    private DataOperations<Battalion> battalionDataOperations;

    @Override
    public void init(){
        try {
            databaseConnector=new DatabaseConnector("root","root","military");
            commandersDataOperations= CommandersConnector.getInstance(databaseConnector);
            battalionDataOperations= BattalionConnector.getInstance(databaseConnector);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        JSONObject jsonObject=new JSONObject();
        String tableBody="";
        String tableHead="Фамилия Имя Отчество!Звание!Год рождения";
        Battalion battalion=battalionDataOperations.getList().get(0);
        for(Commanders commander:commandersDataOperations.getList()){
            tableBody+=commander.getFullNameOfficer()+"!";
            tableBody+=commander.getRank()+"!";
            tableBody+=commander.getBirth()+"!";
        }
        try {
            jsonObject.put("thead",tableHead);
            jsonObject.put("tbody",tableBody);
            jsonObject.put("number",battalion.getNumbers());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        response.getWriter().print(jsonObject.toString());
    }
}
