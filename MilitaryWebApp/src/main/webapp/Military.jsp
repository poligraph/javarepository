<!DOCTYPE html>
<html lang="ru">
<head>
    <title></title>
    <script type="text/javascript" src="js/jquery-1.8.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<div class=" navbar navbar-fixed-top ">
    <div class="navbar-inner">
        <div class="container">
            <a class="brand pull-left">${number}</a>
            <ul class="nav">
                <li><a href="Start_page.jsp">Главная</a></li>
                <li class="divider-vertical"></li>
                <li class="dropdown active">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Военная часть<b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="#">Батальоны</a></li>
                    <li class="divider"></li>
                    <li class="nav-header">Военнослужащие</li>
                    <li><a href="Officers.jsp">Офицерский состав</a></li>
                    <li><a href="Officers.jsp">Солдаты</a></li>
                </ul>
                </li>
                <li><a href="Search.jsp">Поиск</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="row-fluid" style="padding-top:60px;padding-bottom: 20px">
        <div class="span2">
            <ul class="nav nav-list">
                <li ><a href="#"><i class="icon-star-empty"></i>Первый батальон</a></li>
                <li><a href="#"><i class="icon-star-empty"></i>Второй батальон</a></li>
                <li><a href="#"><i class="icon-star-empty"></i>Третий батальон</a></li>
            </ul>
        </div>
        <div class="span8" style="text-align: justify" >
            <style type="text/css">
            tbody tr:hover{
                background-color: rgba(33,33,33,0.2);
                cursor:pointer;
            }
            </style>
        </div>
</div>
</body>
</html>