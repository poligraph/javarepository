<%--
  Created by IntelliJ IDEA.
  User: Evgeniy
  Date: 29.11.12
  Time: 10:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title></title>
    <script type="text/javascript" src="js/jquery-1.8.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/AddRecord.js"></script>
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css">
    <style type="text/css">
        .control-label {
            font-weight: bold;
            font-size: 20px;
        }
    </style>
</head>
<body>
<div class=" navbar navbar-fixed-top ">
    <div class="navbar-inner">
        <div class="container">
            <a class="brand pull-left" id="number"></a>
            <ul class="nav">
                <li><a href="Start_page.jsp">Главная</a></li>
                <li class="divider-vertical"></li>
                <li class="dropdown active">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Военная часть<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="ExcelExport">Экспорт в Excel</a></li>
                        <li class="divider"></li>
                        <li class="nav-header">Военнослужащие</li>
                        <li><a href="Officers.jsp">Офицерский состав</a></li>
                        <li class="active"><a href="#">Солдаты</a></li>
                    </ul>
                </li>
                <li><a href="Search.jsp">Поиск</a></li>
                <li><a href="Help.jsp">Справка</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="row-fluid" style="padding-top:60px;padding-bottom: 20px">
    <div class="span2">
        <ul class="nav nav-list">
            <li class="nav-header">Просмотр</li>
            <li><a href="Officers.jsp"><i class="icon-star-empty"></i>Офицерский состав</a></li>
            <li><a href="Soldiers.jsp"><i class="icon-star-empty"></i>Солдаты</a></li>
            <li class="divider"></li>
            <li class="nav-header">Действия</li>
            <li class="active"><a href="#"><i class="icon-plus"></i>Добавить</a></li>

        </ul>
    </div>
    <div class="span8" style="text-align: justify">
        <legend>Добавление офицера</legend>
        <label class="control-label" for="fullName">ФИО:
            <input class="input-large" type="text" name="fullName" id="fullName" placeholder="ФИО" required/>
        </label>
        <label class="control-label" for="rank">Звание:
            <input type="text" name="rank" id="rank" placeholder="Звание" required>
        </label>
        <label class="control-label" for="birth">Год рождения:
            <input type="number" name="birth" id="birth" placeholder="Год рождения" required>
        </label>
        <label class="control-label" for="address">Адрес:
            <input type="text" name="address" id="address" placeholder="Адрес" required>
        </label>
        <label class="control-label" for="phone">Телефон:
            <input type="text" name="phone" id="phone" placeholder="Телефон" required>
        </label>
        <label class="control-label" for="photo">Фотография:
            <input type="file" name="photo" id="photo" accept="image/jpg" required>
        </label>
        <br>
        <label class="control-label" for="comments">Примечания:</label>
        <textarea style="resize: none;width: 500px;height: 150px" name="comments" id="comments"></textarea><br>
        <button style="margin-left: 440px" id="addOfficerRecord" class="btn-success">Добавить</button>
    </div>
</div>
</body>
</html>