<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title id="title"></title>
    <script type="text/javascript" src="js/jquery-1.8.1.min.js"></script>
    <script type="text/javascript" src="js/LoadOfficerRecord.js"></script>
    <link type="text/css" rel="stylesheet" href="css/military-style.css">
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/ModalWindowActions.js"></script>
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css">
</head>
<body>
<div class=" navbar navbar-fixed-top ">
    <div class="navbar-inner">
        <div class="container">
            <a class="brand pull-left" id="number"></a>
            <ul class="nav">
                <li><a href="#">Главная</a></li>
                <li class="divider-vertical"></li>
                <li class="dropdown active">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Военная часть<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="ExcelExport">Экспорт в Excel</a></li>
                        <li class="divider"></li>
                        <li class="nav-header">Военнослужащие</li>
                        <li><a href="Officers.jsp">Офицерский состав</a></li>
                        <li><a href="Officers.jsp">Солдаты</a></li>
                    </ul>
                </li>
                <li><a href="Search.jsp">Поиск</a></li>
                <li><a href="Help.jsp">Справка</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="row-fluid" style="padding-top:60px;padding-bottom: 20px">
    <div class="span2">
        <ul class="nav nav-list">
            <li class="nav-header">Действия</li>
            <li><a href="#changeModal" data-toggle="modal"><i class="icon-edit"></i>Изменить</a></li>
            <li><a href="#removeModal" data-toggle="modal"><i class="icon-remove"></i>Удалить</a></li>
        </ul>
    </div>
    <div class="span8" style="text-align: justify" >
        <legend id="fullName"></legend>
        <img class="img-polaroid photo" id="photo"/>
        <p class="labels" id="rank"></p>
        <p class="labels" id="birth"></p>
        <p class="labels" id="address"></p>
        <p class="labels" id="phone"></p>
        <p class="labels" id="comments"></p>
    </div>
</div>
<div class="modal fade hide" id="changeModal" style="width: 500px;margin-left: -300px">
    <div class="modal-header">
        <h3>Изменить</h3>
    </div>
    <div class="modal-body">
        <label class="control-label" for="newFullName">ФИО:
            <input type="text" id="newFullName" placeholder="ФИО" required>
        </label>
        <label class="control-label" for="newRank">Звание:
            <input type="text" id="newRank" placeholder="Звание" required>
        </label>
        <label class="control-label" for="newBirth">Год рождения:
            <input type="text" id="newBirth" placeholder="Год рождения" required>
        </label>
        <label class="control-label" for="newAddress">Адрес:
            <input type="text" id="newAddress" placeholder="Адрес" required>
        </label>
        <label class="control-label" for="newPhone">Телефон:
            <input type="text" id="newPhone" placeholder="Телефон" required>
        </label>
        <label class="control-label" for="newComments">Примечания:</label>
            <textarea id="newComments" placeholder="Примечания" style="resize: none;width: 90%"></textarea>
    </div>
    <div class="modal-footer">
        <button id="cancel"  class="btn" data-dismiss="modal">Отмена</button>
        <button id="change"  class="btn btn-success" data-dismiss="modal">Сохранить</button>
    </div>
</div>
<div class="modal fade hide" id="removeModal">
    <div class="modal-header">
        <h3>Удаление</h3>
    </div>
    <div class="modal-body">
        <p>Вы действительно хотите удалить запись?</p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal">Нет</button>
        <button id="remove" class="btn btn-danger" data-dismiss="modal">Да</button>
    </div>
</div>

</body>
</html>