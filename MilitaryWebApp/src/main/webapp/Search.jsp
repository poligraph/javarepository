<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title></title>
    <script type="text/javascript" src="js/jquery-1.8.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/Search.js"></script>
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="css/military-style.css">
</head>
<body>
<div class=" navbar navbar-fixed-top ">
    <div class="navbar-inner">
        <div class="container">
            <a class="brand pull-left"></a>
            <ul class="nav">
                <li><a href="Start_page.jsp">Главная</a></li>
                <li class="divider-vertical"></li>
                <li class="dropdown ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Военная часть<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="ExcelExport">Экспорт в Excel</a></li>
                        <li class="divider"></li>
                        <li class="nav-header">Военнослужащие</li>
                        <li><a href="Officers.jsp">Офицерский состав</a></li>
                        <li><a href="Soldiers.jsp">Солдаты</a></li>
                    </ul>
                </li>
                <li class="active"><a href="">Поиск</a></li>
                <li><a href="Help.jsp">Справка</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="row-fluid" style="padding-top:60px;padding-bottom: 20px">
    <div class="span2">
    </div>
    <div class="span8" style="text-align: justify">
        <legend>Поиск</legend>
        <label class="control-label" for="search">
            <input class="input-large" type="text" name="search" id="search" placeholder="Поиск" required/>
            <button class="btn-success" id="startSearch">Поиск</button>
        </label>
        <table id="officers" class="table table-bordered">
            <thead id="officersTableHead">
            </thead>
            <tbody id="officersTableBody">

            </tbody>
        </table>
        <table id="soldiers" class="table table-bordered">
            <thead id="SoldiersTableHead">
            </thead>
            <tbody id="SoldiersTableBody">
            </tbody>
        </table>
    </div>
</div>
</body>
</html>