<%--
  Created by IntelliJ IDEA.
  User: Evgeniy
  Date: 27.11.12
  Time: 20:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title></title>
    <script type="text/javascript" src="js/jquery-1.8.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="css/military-style.css">
    <script type="text/javascript" src="js/LoadAllSoldiers.js"></script>
</head>
<body>
<div class=" navbar navbar-fixed-top ">
    <div class="navbar-inner">
        <div class="container">
            <a class="brand pull-left" id="number"></a>
            <ul class="nav">
                <li><a href="Start_page.jsp">Главная</a></li>
                <li class="divider-vertical"></li>
                <li class="dropdown active">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Военная часть<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="ExcelExport">Экспорт в Excel</a></li>
                        <li class="divider"></li>
                        <li class="nav-header">Военнослужащие</li>
                        <li><a href="Officers.jsp">Офицерский состав</a></li>
                        <li class="active"><a href="#">Солдаты</a></li>
                    </ul>
                </li>
                <li><a href="Search.jsp">Поиск</a></li>
                <li><a href="Help.jsp">Справка</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="row-fluid" style="padding-top:60px;padding-bottom: 20px">
    <div class="span2">
        <ul class="nav nav-list">
            <li class="nav-header">Просмотр</li>
            <li><a href="Officers.jsp"><i class="icon-star-empty"></i>Офицерский состав</a></li>
            <li class="active"><a href=""><i class="icon-star-empty"></i>Солдаты</a></li>
            <li class="divider"></li>
            <li class="nav-header">Действия</li>
            <li><a href="AddSoldier.jsp"><i class="icon-plus"></i>Добавить служащего</a></li>
        </ul>
    </div>
    <div class="span8" style="text-align: justify" >
        <table class="table table-bordered">
            <thead id="tableHead">
            </thead>
            <tbody id="tableBody">
            </tbody>
        </table>
    </div>
</div>
</body>
</html>