/**
 * Created with IntelliJ IDEA.
 * User: evgenii
 * Date: 04.12.12
 * Time: 8:59
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function () {
    getPlatoonList();
    $("#addOfficerRecord").click(function () {
        addOfficerRecord();
    });
    $("#addSoldierRecord").click(function () {
        addSoldierRecord();
    });
});
function addOfficerRecord() {
    var fullName = $("#fullName").val();
    var rank = $("#rank").val();
    var birth = $("#birth").val();
    var address = $("#address").val();
    var phone = $("#phone").val();
    var comments = $("#comments").val();
    var photo =$("#photo").val();
    photo=photo.split("\\");
    photo="photo/"+photo[photo.length-1];
    $.get("AddOfficer", {"fullName": fullName,"photo":photo, "rank": rank, "birth": birth, "address": address, "phone": phone, "comments": comments});
    window.location.href="/MilitaryWebApp/Officers.jsp";
}
function getPlatoonList(){
    $.post("AddSoldier",function(data){
        data=data.split("!");
        for(var i=0;i<data.length-1;i++){
            $("#platoon").append($("<option>"+data[i]+"</option>"));
        }
    });
}
function addSoldierRecord() {
    var fullName=$("#fullName").val();
    var rank=$("#rank").val();
    var start=$("#start").val();
    var kind=$("#kind").val();
    var platoon=$("#platoon").val();
    var passport=$("#passport").val();
    var department=$("#department").val();
    var photo=$("#photo").val();
    var comments=$("#comments").val();
    photo=photo.split("\\");
    photo="photo/"+photo[photo.length-1];
    if(kind==="Контрактная"){
        kind=1;
    }
    else{
    kind=0;
    }
    $.get("AddSoldier",{"fullName":fullName,"rank":rank,"start":start,"kind":kind,"platoon":platoon,"passport":passport,
    "department":department,"photo":photo,"comments":comments},function(){
        window.location.href="/MilitaryWebApp/Soldiers.jsp";
    });
}