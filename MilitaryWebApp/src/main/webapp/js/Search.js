/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 08.12.12
 * Time: 14:24
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function(){
    $("#startSearch").click(function(){
        searchRecord();
    });
});
function searchRecord(){
    var search=$("#search").val();
    $.get("Search",{"search":search},function(data){
        $("#officersTableBody").html("");
        $("#SoldiersTableBody").html("");
        $("#SoldiersTableHead").html("");
        $("#officersTableHead").html("");
        data=JSON.parse(data);
        data.officersTableHead=data.officersTableHead.split("!");
        data.soldiersTableHead=data.soldiersTableHead.split("!");
        if(data.officersTableBody.length>0){
        $("#officersTableHead").append("<tr><th>"+data.officersTableHead[0]+"</th><th>"+data.officersTableHead[1]+"</th><th>"+data.officersTableHead[2]+"</th></tr>");
        }
        if(data.soldiersTableBody.length>0){
        $("#SoldiersTableHead").append("<tr><th>"+data.soldiersTableHead[0]+"</th><th>"+data.soldiersTableHead[1]+"</th><th>"+data.soldiersTableHead[2]+"</th></tr>");
        }
        $("#officersTableBody").append("<tr>"+data.officersTableBody+"</tr>");
        $("#SoldiersTableBody").append("<tr>"+data.soldiersTableBody+"</tr>");
        $("#SoldiersTableBody tr").click(function(){
            soldierName=$(this).children("td").html();
            window.sessionStorage.setItem("fullName",soldierName);
            window.open("SoldierRecord.jsp","_blank","");
        });
        $("#officersTableBody tr").click(function(){
            officerName=$(this).children("td").html();
            window.sessionStorage.setItem("officerName",officerName);
            window.open("OfficerRecord.jsp","blank","");
        });
    });
}