/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 01.12.12
 * Time: 10:22
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function(){
    var fullNameSoldier = window.sessionStorage.getItem("fullName");
    $.post("ShowSoldierRecord",{"fullNameSoldier":fullNameSoldier},function(data){
        data=JSON.parse(data);
        $("#fullName").append(data.fullName);
        $("#rank").append("Звание: "+data.rank);
        $("#birth").append("Год рождения: "+data.birth);
        $("#start").append("Начало службы: "+data.start);
        $("#platoonName").append("Взвод: "+data.platoonName);
        $("#passport").append("Номер билета: "+data.passport);
        $("#department").append("Номер отделения: "+data.department);
        $("#kind").append("Вид службы: "+data.kind);
        $("#comments").append("Примечания: "+data.comments);
        $("#title").append(data.fullName);
        $("#photo").attr("src",data.photo);

    });
});