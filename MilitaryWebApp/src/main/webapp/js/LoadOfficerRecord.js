/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 30.11.12
 * Time: 20:38
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function(){
    officerName=window.sessionStorage.getItem("officerName");
    $.post("ShowOfficerRecord",{"officerName":officerName},function(data){
        data=JSON.parse(data);
        $("#fullName").append(data.fullName);
        $("#rank").append("Звание: "+data.rank);
        $("#birth").append("Год рождения: "+data.birth);
        $("#address").append("Адрес: "+data.address);
        $("#phone").append("Телефон: "+data.phone);
        $("#comments").append("Примечания: "+data.comments);
        $("#photo").attr("src",data.photo);
        $("#title").append(data.fullName);
    });
});