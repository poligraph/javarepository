/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 02.12.12
 * Time: 15:42
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function(){
    $("#cancel").click(function(){
        clearWindowChangeOfficer();
    });
    $("#change").click(function(){
        changeOfficerData("change");
    });
    $("#remove").click(function(){
        changeOfficerData("remove");
    });
    $("#back").click(function(){
        clearWindowChangeSoldier();
    });
    $("#changeRecord").click(function(){
        changeSolderData("change")
    });
    $("#removeSoldierRecord").click(function(){
        changeSolderData("remove");
    });
});
function clearWindowChangeOfficer(){
    $("#newFullName").val("");
    $("#newRank").val("");
    $("#newPhone").val("");
    $("#newComments").val("");
    $("#newAddress").val("");
    $("#newBirth").val("");

}
function clearWindowChangeSoldier(){
    $("#newFullName").val("");
    $("#newRank").val("");
    $("#newStart").val("");
    $("#newPassport").val("");
    $("#newDepartment").val("");
    $("#newComments").val("");

}
function changeSolderData(action){
    var fullName=$("#fullName").html();
    var newFullName=$("#newFullName").val();
    var newRank=$("#newRank").val();
    var newStart=$("#newStart").val();
    var newKind=$("#newKind").val();
    var newPassport=$("#newPassport").val();
    var newDepartment=$("#newDepartment").val();
    var newComments=$("#newComments").val();
    if(newKind==="Контрактная"){
        newKind=1;
    }
    else{
        newKind=0;
    }
    $.get("ShowSoldierRecord",{"action":action,"fullName":fullName,"newFullName":newFullName,"newRank":newRank,"newStart":newStart,
    "newKind":newKind,"newPassport":newPassport,"newDepartment":newDepartment,"newComments":newComments},function(data){
        if(action=='remove'){
            window.close();
        }
    })
}
function changeOfficerData(action){
    var fullName=$("#fullName").html();
    var photo=$("#photo").attr("src");
    var newFullname=$("#newFullName").val();
    var newRank=$("#newRank").val();
    var newPhone=$("#newPhone").val();
    var newBirth=$("#newBirth").val();
    var newAddress=$("#newAddress").val();
    var newComments=$("#newComments").val();
    $.get("ShowOfficerRecord",{"action":action,"fullName":fullName,"newBirth":newBirth,"newFullName":newFullname,"newRank":newRank,"newPhone":newPhone,
    "newAddress":newAddress,"newComments":newComments,"newPhoto":photo},function(data){
        if(action=='remove'){
            window.close();
        }
    });
}
