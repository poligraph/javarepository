/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 29.11.12
 * Time: 19:49
 * To change this template use File | Settings | File Templates.
 */
var officerName;
$(document).ready(function(){
    $.get("ViewOfficers",function(data){
        jsonData=JSON.parse(data);
        tablebody=jsonData.tbody.split("!");
        tablehead=jsonData.thead.split("!");
        number=jsonData.number;
        resultHead="<tr><th>"+tablehead[0]+"</th><th>"+tablehead[1]+"</th><th>"+tablehead[2]+"</th></tr>";
        for(var i= 0,resultStr="";i<tablebody.length-3;i+=3){
            resultStr+="<tr>";
            resultStr+="<td>"+tablebody[i]+"</td>";
            resultStr+="<td>"+tablebody[i+1]+"</td>";
            resultStr+="<td>"+tablebody[i+2]+"</td>";
            resultStr+="</tr>";
            $("#tableBody").append(resultStr);
            resultStr="";
        }
        $("#tableHead").append(resultHead);
        $("#number").append(number);
        $("table tbody tr").click(function(){
            officerName=$(this).children("td").html();
            window.sessionStorage.setItem("officerName",officerName);
            window.open("OfficerRecord.jsp","blank","");
        });
    });
});