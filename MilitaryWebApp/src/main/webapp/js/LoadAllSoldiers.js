/**
 * Created with IntelliJ IDEA.
 * User: Evgeniy
 * Date: 30.11.12
 * Time: 10:51
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function(){
    $.get("ViewSoldiers",function(data){
        jsonData=JSON.parse(data);
        tablebody=jsonData.tbody.split("!");
        tablehead=jsonData.thead.split("!");
        resultHead="<tr><th>"+tablehead[0]+"</th><th>"+tablehead[1]+"</th><th>"+tablehead[2]+"</th></tr>";
        resultBody="";
        number=jsonData.number;
        for(var i=0;i<tablebody.length-3;i+=3){
            resultBody+="<tr>";
            resultBody+="<td>"+tablebody[i]+"</td>";
            resultBody+="<td>"+tablebody[i+1]+"</td>";
            resultBody+="<td>"+tablebody[i+2]+"</td>";
            resultBody+="</tr>";
            $("#tableBody").append(resultBody);
            resultBody="";
        }
        $("#tableHead").append(resultHead);
        $("#number").append(number);
        $("table tbody tr").click(function(){
            soldierName=$(this).children("td").html();
            window.sessionStorage.setItem("fullName",soldierName);
            window.open("SoldierRecord.jsp","_blank","");
        });
    });
});