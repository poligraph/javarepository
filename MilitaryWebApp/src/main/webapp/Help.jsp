<%--
  Created by IntelliJ IDEA.
  User: Evgeniy
  Date: 29.11.12
  Time: 10:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title></title>
    <script type="text/javascript" src="js/jquery-1.8.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css">
    <style>
        img{
            margin-bottom: 15px;
            margin-top: 15px;
        }
    </style>
</head>
<body>
<div class=" navbar navbar-fixed-top ">
    <div class="navbar-inner">
        <div class="container">
            <a class="brand pull-left"></a>
            <ul class="nav">
                <li><a href="Start_page.jsp">Главная</a></li>
                <li class="divider-vertical"></li>
                <li class="dropdown ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Военная часть<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="ExcelExport">Экспорт в Excel</a></li>
                        <li class="divider"></li>
                        <li class="nav-header">Военнослужащие</li>
                        <li><a href="Officers.jsp">Офицерский состав</a></li>
                        <li><a href="Soldiers.jsp">Солдаты</a></li>
                    </ul>
                </li>
                <li><a href="Search.jsp">Поиск</a></li>
                <li class="active"><a href="">Справка</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="row-fluid" style="padding-top:60px;padding-bottom: 20px;">
    <div class="span2">
        <ul class="nav nav-list" style="position: fixed;">
            <li class="nav-header">Разделы</li>
            <li><a href="#review"><i class="icon-book"></i>Просмотр</a></li>
            <li><a href="#addition"><i class="icon-book"></i>Добавление</a></li>
            <li><a href="#removal"><i class="icon-book"></i>Удаление</a></li>
            <li><a href="#change"><i class="icon-book"></i>Изменение</a></li>
            <li><a href="#search"><i class="icon-book"></i>Поиск</a></li>
            <li><a href="#export"><i class="icon-book"></i>Экспорт</a></li>

        </ul>
    </div>
    <div class="span8" style="text-align: justify">
        <legend>Справка</legend>
        <a name="review"></a>
        <h2>Просмотр</h2>
        Для просмотра военнослужащих необходимо щелчком мыши развернуть пункт меню навигации
        "Военная часть", в разделе военнослужащие выбрать необходимый раздел военнослужащих.
        После проделанных действий пользователь увидит таблицу военнослужащих. Для просмотра
        детальной информации о военнослужащем следует щелкнуть в таблице по интересующей
        записи. После чего в новой вкладке откроется детальная информация о военнослужащем.
        <img src="img/Help/Record.jpg" class="img-polaroid">
        <a name="addition"></a>
        <h2>Добавление</h2>
        Для добавления нового военнослужащего требуется выбрать в меню навигации пункт
        "Военная часть"->Солдаты в меню действия выбрать Добавить служащего. После чего заполнить
        поля информации о военнослужащем, и нажать кнопку Добавить.
        <img src="img/Help/Add.jpg" class="img-polaroid">
        <a name="removal"></a>
        <h2>Удаление</h2>
        Для того чтобы удалить запись о военнослужащем необходимо открыть подробную информацию
        о военнослужащем, из меню Действия  выбрать Удалить, после чего появится всплывающее окно
        подтверждения удаления, выберите Да. Используйте данную функцию осторожно, после удаления
        информация будет полностью утеряна.
        <img src="img/Help/Remove.jpg" class="img-polaroid">
        <h2>Измение</h2>
        <a name="change"></a>
        Для изменения информации о военнослужащем необходимо открыть соответствующую запись, в
        меню Действия выбрать пункт Изменить, после чего внести информацию о военнослужащем и
        нажать кнопку Сохранить.
        <img src="img/Help/change.jpg" class="img-polaroid">
        <a name="search"></a>
        <h2>Поиск</h2>
        Для поиска интересующих военнослужащих необходимо на панели навигации выбрать пункт
        Поиск, задать критерии поиска и нажать кпопку  Поиск.
        <img src="img/Help/Help.jpg" class="img-polaroid">
        <a name="export"></a>
        <h2>Экспорт</h2>
        Для экспорта данных необходимо перейти в Военная часть->Экспорт в Excel, после чего появится
        окно загружки Excel файла, необходимо сохранить его на локальном жестком диске.
        <img src="img/Help/Export.jpg" class="img-polaroid">

    </div>
</div>
</body>
</html>